module Main exposing (main)

import Application
import Backend exposing (Authentication(..), Backend, Role(..), retrieveUser)
import BrightnessStorage exposing (Brightness(..), parseBrightness, rememberDarkmode, rememberLightmode)
import Browser
import Browser.Navigation as Nav exposing (Key)
import Html
import Platform.Cmd exposing (Cmd)
import Task
import Time
import Url
import Utils exposing (withCommand, withCommands, withoutCommands)


type alias Flags =
    { backendUrl : String
    , brightness : String
    }


type alias Model =
    { application : Maybe Application.Model
    , key : Key
    , backend : Backend
    , brightness : Brightness
    , timezone : Time.Zone
    }


type Msg
    = Url_requested Browser.UrlRequest
    | Url_changed Url.Url
    | GotAuthentication Url.Url (Result String Authentication)
    | Got_Timezone Time.Zone
    | ApplicationMsg Application.Msg


init : Flags -> Url.Url -> Key -> ( Model, Cmd Msg )
init flags url key =
    { key = key
    , application = Nothing
    , backend = Backend.fromUrl flags.backendUrl
    , brightness = parseBrightness flags.brightness
    , timezone = Time.utc
    }
        |> withCommands
            [ Backend.retrieveUser (Backend.fromUrl flags.backendUrl) (GotAuthentication url)
            , Task.perform Got_Timezone Time.here
            ]


resolveApplicationIntention : Model -> Application.Intention -> ( Model, Cmd Msg )
resolveApplicationIntention model intention =
    case intention of
        Application.Switch_to_Light ->
            ( { model | brightness = Light }, rememberLightmode )

        Application.Switch_to_Dark ->
            ( { model | brightness = Dark }, rememberDarkmode )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Url_requested request ->
            case request of
                Browser.Internal url ->
                    ( model
                    , if url.path |> String.endsWith ".php" then
                        Nav.load (Url.toString url)

                      else
                        Nav.pushUrl model.key <| Url.toString url
                    )

                Browser.External href ->
                    model |> withCommand (Nav.load href)

        Url_changed url ->
            model.application
                |> Maybe.map (Application.updateUrl model.backend url)
                |> Maybe.map (\( app, cmds ) -> ( { model | application = Just app }, Cmd.map ApplicationMsg cmds ))
                |> Maybe.withDefault ( model, Cmd.none )

        GotAuthentication url authentication ->
            case authentication of
                Ok auth ->
                    Application.init model.backend url auth
                        |> (\( app, cmds ) -> ( { model | application = Just app }, Cmd.map ApplicationMsg cmds ))

                Err _ ->
                    model |> withoutCommands

        Got_Timezone timezone ->
            { model | timezone = timezone } |> withoutCommands

        ApplicationMsg innerMsg ->
            case model.application of
                Just application ->
                    let
                        ( app, cmds, intention ) =
                            Application.update model.backend innerMsg application
                    in
                    intention
                        |> Maybe.map (resolveApplicationIntention model)
                        |> Maybe.withDefault ( model, Cmd.none )
                        |> (\( newModel, cmd ) -> ( { newModel | application = Just app }, Cmd.batch [ cmd, Cmd.map ApplicationMsg cmds ] ))

                Nothing ->
                    model |> withoutCommands


subscriptions : Model -> Sub Msg
subscriptions model =
    model.application
        |> Maybe.map (Application.subscriptions >> Sub.map ApplicationMsg)
        |> Maybe.withDefault Sub.none


view : Model -> Browser.Document Msg
view model =
    case model.application of
        Just application ->
            Application.view model.timezone model.brightness model.backend application
                |> (\doc -> { title = doc.title, body = doc.body |> List.map (Html.map ApplicationMsg) })

        Nothing ->
            { title = "Welcome"
            , body = []
            }


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = Url_requested
        , onUrlChange = Url_changed
        }
