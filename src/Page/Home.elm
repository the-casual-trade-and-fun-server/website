module Page.Home exposing (Model, Msg, init, subscriptions, update, view)

import Backend exposing (DiscordInfo, PlayerOnServer, ServerInfo, ServerStatus(..), SteamGroup)
import Html exposing (Html, a, button, div, h1, i, img, li, main_, p, span, text)
import Html.Attributes exposing (class, href, src)
import Html.Keyed exposing (ul)
import Time exposing (every)
import View.Button exposing (normalButton)


type RemoteData data
    = Loading
    | Loaded data
    | Error String


type alias Model =
    { serverStatus : RemoteData ServerStatus
    , steamGroup : RemoteData SteamGroup
    , discord : RemoteData DiscordInfo
    }


type Msg
    = ServerStatusLoaded ServerStatus
    | FailedToLoadServerStatus String
    | CheckServerStatusAgainClicked
    | ReloadServerInfoTick
    | SteamGroupLoaded SteamGroup
    | FailedToLoadSteamGroup String
    | DiscordInfoLoaded DiscordInfo
    | FailedToLoadDiscordInfo String


withLoadServerStatus : Backend.Backend -> Model -> ( Model, Cmd Msg )
withLoadServerStatus backend model =
    ( model, Backend.serverStatus backend ServerStatusLoaded FailedToLoadServerStatus )


init : Backend.Backend -> ( Model, Cmd Msg )
init backend =
    ( { serverStatus = Loading
      , steamGroup = Loading
      , discord = Loading
      }
    , Cmd.batch
        [ Backend.serverStatus backend ServerStatusLoaded FailedToLoadServerStatus
        , Backend.steamGroup backend SteamGroupLoaded FailedToLoadSteamGroup
        , Backend.onlineUsersOnDiscord backend DiscordInfoLoaded FailedToLoadDiscordInfo
        ]
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.serverStatus of
        Loaded (Server_is_online _) ->
            every 30000 (\_ -> ReloadServerInfoTick)

        _ ->
            Sub.none


withoutCommands : Model -> ( Model, Cmd Msg )
withoutCommands model =
    ( model, Cmd.none )


withServerStatus : RemoteData ServerStatus -> Model -> Model
withServerStatus status model =
    { model | serverStatus = status }


update : Backend.Backend -> Model -> Msg -> ( Model, Cmd Msg )
update backend model msg =
    case msg of
        ServerStatusLoaded status ->
            model
                |> withServerStatus (Loaded status)
                |> withoutCommands

        FailedToLoadServerStatus error ->
            model
                |> withServerStatus (Error error)
                |> withoutCommands

        CheckServerStatusAgainClicked ->
            model
                |> withServerStatus Loading
                |> withLoadServerStatus backend

        ReloadServerInfoTick ->
            model |> withLoadServerStatus backend

        SteamGroupLoaded members ->
            { model | steamGroup = Loaded members }
                |> withoutCommands

        FailedToLoadSteamGroup error ->
            { model | steamGroup = Error error }
                |> withoutCommands

        DiscordInfoLoaded users ->
            { model | discord = Loaded users }
                |> withoutCommands

        FailedToLoadDiscordInfo error ->
            { model | discord = Error error }
                |> withoutCommands


viewPlayersHeader : Html Msg
viewPlayersHeader =
    div [ class "p-2 bg-neutral-lighter text-neutral-dark shadow flex sticky top-0" ]
        [ div [ class "w-7/12" ] [ text "Player" ]
        , div [ class "w-2/12 text-right" ] [ text "Score" ]
        , div [ class "w-3/12 text-right" ] [ text "Duration" ]
        ]


viewPlayerDuration : Float -> String
viewPlayerDuration duration =
    (duration / 3600 |> floor |> remainderBy 60 |> String.fromInt |> String.padLeft 2 '0')
        ++ ":"
        ++ (duration / 60 |> floor |> remainderBy 60 |> String.fromInt |> String.padLeft 2 '0')
        ++ ":"
        ++ (duration |> floor |> remainderBy 60 |> String.fromInt |> String.padLeft 2 '0')


viewPlayer : PlayerOnServer -> ( String, Html Msg )
viewPlayer player =
    ( player.name
    , li [ class "p-2 flex odd:bg-neutral-lighter text-neutral" ]
        [ div [ class "w-7/12 text-neutral-darker" ] [ text player.name ]
        , div [ class "w-2/12 text-right" ] [ text (String.fromInt player.score) ]
        , div [ class "w-3/12 text-right" ] [ text (viewPlayerDuration player.duration) ]
        ]
    )


viewPlayers : List PlayerOnServer -> Html Msg
viewPlayers players =
    case players of
        [] ->
            div [ class "my-auto text-center text-neutral w-full" ] [ text "No one is playing right now" ]

        onlinePlayers ->
            onlinePlayers
                |> List.sortBy .duration
                |> List.reverse
                |> List.map viewPlayer
                |> List.append [ ( "Header", viewPlayersHeader ) ]
                |> ul [ class "w-full h-56 overflow-y-auto" ]


viewConnectButton : ServerInfo -> Html Msg
viewConnectButton info =
    if (info.players |> List.length) == info.maxPlayers then
        button [ class "w-1/2 text-center button primary rounded-none rounded-br p-5", Html.Attributes.disabled True ]
            [ i [ class "fas fa-heart mr-2" ] []
            , text "FULL OF LOVE"
            ]

    else
        a [ href ("steam://connect/" ++ info.address), class "w-1/2 text-center button primary rounded-none rounded-br p-5" ]
            [ i [ class "fas fa-gamepad mr-2" ] []
            , text
                (if (info.players |> List.length) == 0 then
                    "BE THE FIRST"

                 else
                    "JOIN YOUR FRIENDS"
                )
            ]


viewServerInfo : ServerInfo -> Html Msg
viewServerInfo info =
    div [ class "bg-neutral-lightest rounded shadow-xl w-full max-w-xl" ]
        [ div [ class "flex flex-wrap sm:flex-no-wrap" ]
            [ img
                [ class "w-full sm:w-56 h-56 flex-shrink-0 rounded-tl object-cover object-center"
                , src info.image
                ]
                []
            , viewPlayers info.players
            ]
        , div [ class "flex border-t border-neutral-light" ]
            [ div [ class "p-2 flex-grow-0 w-1/2" ]
                [ div [ class "font-bold truncate" ] [ text info.map ]
                , div []
                    [ text (String.fromInt (info.players |> List.length) ++ "/" ++ String.fromInt info.maxPlayers)
                    , span [ class "ml-1 text-light" ] [ text "players" ]
                    ]
                ]
            , viewConnectButton info
            ]
        ]


viewServerOffline : Html Msg
viewServerOffline =
    div [ class "bg-neutral-lighter rounded shadow-xl w-full max-w-xl flex" ]
        [ span [ class "fa-stack fa-4x" ]
            [ i [ class "fas fa-server fa-stack-1x text-neutral-darker" ] []
            , i [ class "fas fa-slash fa-stack-1x text-neutral" ] []
            ]
        , div [ class "m-auto text-center text-neutral" ]
            [ div [] [ text "Server is not available" ]
            , normalButton "check again" CheckServerStatusAgainClicked
            ]
        ]


viewServerLoading : Html Msg
viewServerLoading =
    div [ class "bg-neutral-lighter rounded shadow-xl w-full max-w-xl flex" ]
        [ i [ class "mx-12 my-8 fas fa-spinner fa-spin fa-4x text-neutral-darker" ] []
        , div [ class "m-auto" ]
            [ div [] [ text "Retrieving Server Status" ]
            ]
        ]


viewServerStatus : RemoteData ServerStatus -> Html Msg
viewServerStatus model =
    case model of
        Loading ->
            viewServerLoading

        Loaded status ->
            case status of
                Server_is_online info ->
                    viewServerInfo info

                Server_is_offline _ ->
                    viewServerOffline

        Error _ ->
            viewServerOffline


asNumber : Maybe Int -> String
asNumber data =
    case data of
        Just number ->
            String.fromInt number

        Nothing ->
            "-"


remoteDataAsMaybe : RemoteData t -> Maybe t
remoteDataAsMaybe data =
    case data of
        Loaded d ->
            Just d

        _ ->
            Nothing


viewDiscordStatus : Maybe DiscordInfo -> Html Msg
viewDiscordStatus discord =
    a [ class "block mr-16 text-neutral-darker hover:text-primary-light", href (discord |> Maybe.map .invite |> Maybe.withDefault "https://discordapp.com/") ]
        [ i [ class "fab fa-discord text-5xl text-discord w-full text-center" ] []
        , p [] [ text "Invite yourself" ]
        , p [ class "text-neutral text-xs" ]
            [ text ((discord |> Maybe.map .online |> asNumber) ++ " online") ]
        ]


groupUrl : RemoteData SteamGroup -> String
groupUrl remoteGroup =
    case remoteGroup of
        Loaded group ->
            "https://steamcommunity.com/groups/" ++ group.id

        _ ->
            "https://steamcommunity.com"


viewJoinSteamGroup : RemoteData SteamGroup -> Html Msg
viewJoinSteamGroup group =
    a [ class "block text-neutral-darker hover:text-primary-light", group |> groupUrl |> href ]
        [ i [ class "fab fa-steam text-5xl text-steam w-full text-center" ] []
        , p [] [ text "Join the group" ]
        , p [ class "text-neutral text-xs" ]
            [ text ((group |> remoteDataAsMaybe |> Maybe.andThen .members |> asNumber) ++ " members") ]
        ]


viewLogo : Html Msg
viewLogo =
    div []
        [ img [ class "w-16 h-16 inline mr-4", src "/assets/logo-day.png" ] []
        , span [ class "text-lg" ] [ text "The Casual Trade and Fun Server" ]
        ]


view : Backend.Backend -> Model -> Html Msg
view backend model =
    main_ [ class "dark text-default p-8 bg-landing min-h-screen" ]
        [ viewLogo
        , div [ class "pt-8 flex flex-col lg:flex-row items-center lg:items-start justify-start lg:justify-center" ]
            [ div [ class "mb-8 w-full md:w-1/2 text-center lg:text-left" ]
                [ h1 [ class "text-2xl" ] [ text "Deep dive into nostalgia" ]
                , p [ class "text-light mb-4" ] [ text "A place of fun, exploration and friends" ]
                , a [ class "button inline-block secondary py-4 px-6", href "/about" ] [ text "Find out more" ]
                , a [ class "button inline-block ml-2 py-4 px-8", href (Backend.loginUrl backend) ] [ i [ class "fab fa-steam mr-2" ] [], text "Login" ]
                , div [ class "flex items-center justify-center lg:justify-start pt-6" ]
                    [ viewDiscordStatus (remoteDataAsMaybe model.discord)
                    , viewJoinSteamGroup model.steamGroup
                    ]
                ]
            , viewServerStatus model.serverStatus
            ]
        ]
