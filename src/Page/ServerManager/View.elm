module Page.ServerManager.View exposing (view)

import Backend exposing (Member, Rank(..), Vip)
import Html exposing (Html, button, div, fieldset, h2, h3, header, i, img, label, option, section, select, span, text)
import Html.Attributes exposing (class, disabled, selected, src, value)
import Html.Events exposing (onClick, onInput)
import Page.ServerManager.Types exposing (CorrectTimeModel, CorrectTimeMsg(..), DeclareUserAsMemberModel, DeclareUserAsMemberMsg(..), Dialog(..), ExtendTimeForAllMsg(..), ExtendTimeForAllVipsModel, ExtendTimeModel, ExtendTimeMsg(..), GiveUserVipModel, GiveUserVipMsg(..), GroupedMembers, Members, Model, Msg(..), SetRankModel, SetRankMsg(..), TransferTimeModel, TransferTimeMsg(..))
import Time
import View.Button exposing (dangerousButton, normalButton, primaryButton, submitButton)
import View.DropdownMenu exposing (MenuItem, dropdownMenu)
import View.DurationInput exposing (isDurationValid, isDurationValidWithLimit)
import View.SteamIdInput


isTransferTimeValid : Int -> TransferTimeModel -> Bool
isTransferTimeValid remainingDays model =
    View.SteamIdInput.isValid model.receiver && isDurationValid model.duration && isDurationValidWithLimit remainingDays model.duration


isExtendTimeValid : ExtendTimeModel -> Bool
isExtendTimeValid duration =
    isDurationValid duration


isExtendTimeForAllVipsValid : ExtendTimeForAllVipsModel -> Bool
isExtendTimeForAllVipsValid duration =
    isDurationValid duration


isCorrectTimeValid : CorrectTimeModel -> Bool
isCorrectTimeValid duration =
    isDurationValid duration


isDeclareUserAsMemberValid : DeclareUserAsMemberModel -> Bool
isDeclareUserAsMemberValid model =
    View.SteamIdInput.isValid model.user


isGiveUserVipValid : GiveUserVipModel -> Bool
isGiveUserVipValid model =
    View.SteamIdInput.isValid model.user && isDurationValid model.duration


monthAsNumeric : Time.Month -> String
monthAsNumeric month =
    case month of
        Time.Jan ->
            "01"

        Time.Feb ->
            "02"

        Time.Mar ->
            "03"

        Time.Apr ->
            "04"

        Time.May ->
            "05"

        Time.Jun ->
            "06"

        Time.Jul ->
            "07"

        Time.Aug ->
            "08"

        Time.Sep ->
            "09"

        Time.Oct ->
            "10"

        Time.Nov ->
            "11"

        Time.Dec ->
            "12"


posixAsString : Time.Zone -> Time.Posix -> String
posixAsString timezone posix =
    (String.padLeft 2 '0' <| String.fromInt <| Time.toDay timezone posix)
        ++ "."
        ++ (monthAsNumeric <| Time.toMonth timezone posix)
        ++ "."
        ++ (String.fromInt <| Time.toYear timezone posix)


addMinutes : Int -> Time.Posix -> Time.Posix
addMinutes minutes time =
    time
        |> Time.posixToMillis
        |> (+) (minutes * 60000)
        |> Time.millisToPosix


expirationDate : Vip -> Time.Posix
expirationDate vip =
    vip.date |> addMinutes (vip.duration + vip.duraitonFromPauses)


daysNumericText : Int -> Html msg
daysNumericText days =
    span []
        [ span [ class "text-default" ] [ text (String.fromInt days) ]
        , span [ class "text-light" ]
            [ text
                (if days == 1 then
                    " day"

                 else
                    " days"
                )
            ]
        ]


viewExpirationDate : Vip -> Time.Zone -> Html msg
viewExpirationDate vip timezone =
    case vip.pausedSince of
        Just _ ->
            span [ class "text-secondary-lighter" ] [ text "paused" ]

        Nothing ->
            div [ class "text-light" ]
                [ text "expires on "
                , span [ class "text-default" ] [ text <| posixAsString timezone (expirationDate vip) ]
                ]


viewVip : Time.Zone -> ( Vip, List (MenuItem Msg) ) -> Html Msg
viewVip timezone ( vip, menuitems ) =
    div [ class "p-4 m-4 flex-shrink-0 flex max-w-xs w-full" ]
        [ img [ class "w-16 h-16 flex-shrink-0 mr-4 rounded-full shadow-outline-soft", src vip.avatar ] []
        , div []
            [ div [ class "font-bold flex" ] [ text vip.name, dropdownMenu menuitems ]
            , div [ class "text-light" ]
                [ minutesAsDays vip.duration |> daysNumericText
                , if vip.duraitonFromPauses > 0 then
                    span []
                        [ text " (+"
                        , vip.duraitonFromPauses |> minutesAsDays |> daysNumericText
                        , text " from pauses)"
                        ]

                  else
                    text ""
                ]
            , viewExpirationDate vip timezone
            ]
        ]


nameOfRank : Rank -> String
nameOfRank rank =
    case rank of
        Owner ->
            "Owner"

        Admin ->
            "Admin"

        Moderator ->
            "Moderator"

        Mapper ->
            "Mapper"

        PermanentVip ->
            "Permanent VIP"


viewMember : ( Member, List (MenuItem Msg) ) -> Html Msg
viewMember ( member, menuitems ) =
    div [ class "w-48 mx-8 my-6 flex-shrink-0" ]
        [ img [ class "mx-auto w-16 h-16 rounded-full shadow-outline-soft", src member.avatar ] []
        , div [ class "font-bold flex justify-center" ]
            [ text member.name
            , dropdownMenu menuitems
            ]
        ]


viewMemberGroup : String -> Members -> Html Msg
viewMemberGroup group members =
    div []
        [ h3 [ class "font-bold mt-10 text-light" ] [ text group ]
        , div [ class "flex flex-wrap" ] (members |> List.map viewMember)
        ]


viewMembers : GroupedMembers -> Html Msg
viewMembers members =
    div []
        ([ members.owners |> Maybe.map (viewMemberGroup "Owner")
         , members.admins |> Maybe.map (viewMemberGroup "Admin")
         , members.moderators |> Maybe.map (viewMemberGroup "Moderator")
         , members.mappers |> Maybe.map (viewMemberGroup "Mapper")
         , members.permanentVips |> Maybe.map (viewMemberGroup "Permanent VIP")
         ]
            |> List.filterMap identity
        )


viewDurationWithLimit : Int -> Int -> Html TransferTimeMsg
viewDurationWithLimit limit duration =
    View.DurationInput.viewWithLimit DurationToTransferChanged limit duration


viewGiveUserVip : GiveUserVipModel -> Html GiveUserVipMsg
viewGiveUserVip model =
    div []
        [ fieldset []
            [ label [ class "form-label" ] [ text "User" ]
            , View.SteamIdInput.view UserChanged model.user
            ]
        , fieldset []
            [ label [ class "form-label" ] [ text "Duration" ]
            , View.DurationInput.view DurationChanged model.duration
            ]
        ]


viewTransferTime : Time.Posix -> Vip -> TransferTimeModel -> Html TransferTimeMsg
viewTransferTime time vip model =
    div []
        [ fieldset []
            [ label [ class "form-label mr-1" ] [ text "Receiver" ]
            , View.SteamIdInput.view ReceiverChanged model.receiver
            ]
        , fieldset []
            [ label [ class "form-label" ] [ text "Duration" ]
            , viewDurationWithLimit (remainingDurationInDays time vip) model.duration
            ]
        ]


viewExtendTime : ExtendTimeModel -> Html ExtendTimeMsg
viewExtendTime duration =
    fieldset []
        [ label [ class "form-label" ] [ text "Extra duration of" ]
        , View.DurationInput.view DurationToExtendChanged duration
        ]


viewExtendTimeForAllVips : ExtendTimeForAllVipsModel -> Html ExtendTimeForAllMsg
viewExtendTimeForAllVips duration =
    fieldset []
        [ label [ class "form-label" ] [ text "Duration" ]
        , View.DurationInput.view DurationToExtendForAllChanged duration
        ]


viewCorrectTime : CorrectTimeModel -> Html CorrectTimeMsg
viewCorrectTime duration =
    fieldset []
        [ label [ class "form-label" ] [ text "New duration:" ]
        , View.DurationInput.view CorrectDurationChanged duration
        ]


subtractTime : Time.Posix -> Time.Posix -> Time.Posix
subtractTime timeToSubtract time =
    (time |> Time.posixToMillis)
        - (timeToSubtract |> Time.posixToMillis)
        |> Time.millisToPosix


minutesAsDays : Int -> Int
minutesAsDays minutes =
    minutes // 1440


asDays : Time.Posix -> Int
asDays time =
    time
        |> Time.posixToMillis
        |> toFloat
        |> (\millis -> millis / 86400000)
        |> floor


remainingDurationInDays : Time.Posix -> Vip -> Int
remainingDurationInDays time vip =
    case vip.pausedSince of
        Just pausedSince ->
            pausedSince
                |> subtractTime vip.date
                |> asDays
                |> (\daysFromStartTillPause -> (vip.duration + vip.duraitonFromPauses |> minutesAsDays) - daysFromStartTillPause)

        Nothing ->
            expirationDate vip
                |> subtractTime time
                |> asDays


viewRankSelection : Rank -> Html DeclareUserAsMemberMsg
viewRankSelection selectedRank =
    select [ class "border form-element", onInput RankSelected_ ]
        [ option [ value "owner", selected (selectedRank == Owner) ] [ text "Owner" ]
        , option [ value "admin", selected (selectedRank == Admin) ] [ text "Admin" ]
        , option [ value "moderator", selected (selectedRank == Moderator) ] [ text "Moderator" ]
        , option [ value "mapper", selected (selectedRank == Mapper) ] [ text "Mapper" ]
        , option [ value "permanent-vip", selected (selectedRank == PermanentVip) ] [ text "Permanent VIP" ]
        ]


viewDeclareUserAsMember : DeclareUserAsMemberModel -> Html DeclareUserAsMemberMsg
viewDeclareUserAsMember model =
    div []
        [ fieldset []
            [ label [ class "form-label" ] [ text "User:" ]
            , View.SteamIdInput.view UserChanged_ model.user
            ]
        , fieldset []
            [ label [ class "form-label" ] [ text "Rank:" ]
            , viewRankSelection model.rank
            ]
        ]


viewRankInput : Rank -> Rank -> Html SetRankMsg
viewRankInput memberRank selectedRank =
    select [ class "border form-element", onInput RankSelected ]
        [ option [ value "owner", disabled (memberRank == Owner), selected (selectedRank == Owner) ] [ text "Owner" ]
        , option [ value "admin", disabled (memberRank == Admin), selected (selectedRank == Admin) ] [ text "Admin" ]
        , option [ value "moderator", disabled (memberRank == Moderator), selected (selectedRank == Moderator) ] [ text "Moderator" ]
        , option [ value "mapper", disabled (memberRank == Mapper), selected (selectedRank == Mapper) ] [ text "Mapper" ]
        , option [ value "permanent-vip", disabled (memberRank == PermanentVip), selected (selectedRank == PermanentVip) ] [ text "Permanent VIP" ]
        ]


viewSetRankOfMember : Member -> SetRankModel -> Html SetRankMsg
viewSetRankOfMember member rank =
    fieldset []
        [ label [] [ text "New rank:" ]
        , viewRankInput member.rank rank
        ]


type DialogContent
    = Form String String (Maybe Msg) (Html Msg)
    | Question (Html Msg) String Msg
    | DangerousQuestion (Html Msg) String Msg


dialogContent : Dialog -> DialogContent
dialogContent dialog =
    case dialog of
        GiveUserVip model ->
            Form
                "Give user VIP"
                "Give VIP"
                (if isGiveUserVipValid model then
                    Just GiveUserVipSubmitted

                 else
                    Nothing
                )
                (viewGiveUserVip model |> Html.map GiveUserVipMsg)

        TransferTime time vip model ->
            Form
                ("Sending duration from " ++ vip.name)
                "Transfer time"
                (if isTransferTimeValid (remainingDurationInDays time vip) model then
                    Just TransferTimeSubmitted

                 else
                    Nothing
                )
                (viewTransferTime time vip model |> Html.map TransferTimeMsg)

        ExtendTime vip model ->
            Form
                ("Extending time of " ++ vip.name)
                "Extend time"
                (if isExtendTimeValid model then
                    Just ExtendTimeSubmitted

                 else
                    Nothing
                )
                (viewExtendTime model |> Html.map ExtendTimeMsg)

        ExtendTimeForAllVips model ->
            Form
                "Extending time for all VIPs"
                "Extend for all"
                (if isExtendTimeForAllVipsValid model then
                    Just ExtendTimeForAllVipsSubmitted

                 else
                    Nothing
                )
                (viewExtendTimeForAllVips model |> Html.map ExtendTimeForAllMsg)

        CorrectTime vip model ->
            Form
                ("Correct time of " ++ vip.name)
                "Correct time"
                (if isCorrectTimeValid model then
                    Just CorrectTimeSubmitted

                 else
                    Nothing
                )
                (viewCorrectTime model |> Html.map CorrectTimeMsg)

        ConfirmPausing vip ->
            Question
                (span [] [ text "Should the time of ", span [ class "font-bold" ] [ text vip.name ], text " be paused?" ])
                "Pause time"
                (ConfirmPauseClicked vip)

        ConfirmResuming vip ->
            Question
                (span [] [ text "Should the time of ", span [ class "font-bold" ] [ text vip.name ], text " be resumed?" ])
                "Resume time"
                (ConfirmResumeClicked vip)

        ConfirmRemoveDueToRuleBreaking vip ->
            DangerousQuestion
                (span [] [ text "Should ", span [ class "font-bold" ] [ text vip.name ], text " be removed as VIP due to rule breaking?" ])
                "Remove"
                RemoveDueToRuleBreakingConfirmed

        DeclareUserAsMember model ->
            Form
                "Adding a new member"
                "Add Member"
                (if isDeclareUserAsMemberValid model then
                    Just DeclareUserAsMemberSubmitted

                 else
                    Nothing
                )
                (viewDeclareUserAsMember model |> Html.map DeclareUserAsMemberMsg)

        SetRank member model ->
            Form
                ("Set rank of " ++ member.name ++ " (" ++ nameOfRank member.rank ++ ")")
                "Set Rank"
                (Just SetRankOfMemberSubmitted)
                (viewSetRankOfMember member model |> Html.map SetRankMsg)

        ConfirmRemoveMember member ->
            DangerousQuestion
                (span [] [ text "Should ", span [ class "font-bold" ] [ text member.name ], text " be removed from members?" ])
                "Remove Member"
                ConfirmRemoveMemberClicked


viewDialogDismissButton : Html Msg
viewDialogDismissButton =
    i [ onClick DialogDismissingClicked, class "fas fa-times ml-6 text-base font-bold text-light hover:text-lighter cursor-pointer" ] []


viewDialogModal : String -> List (Html Msg) -> Html Msg
viewDialogModal title content =
    div [ class "modal" ]
        [ div [ class "modal-box" ]
            (h2 [ class "mb-4 text-light font-bold flex items-baseline justify-between" ] [ text title, viewDialogDismissButton ]
                :: content
            )
        ]


viewQuestionModal : Html Msg -> List (Html Msg) -> Html Msg
viewQuestionModal question buttons =
    div [ class "modal" ]
        [ div [ class "modal-box wider" ]
            (h2 [ class "mb-8 flex items-baseline justify-between" ]
                [ question
                , viewDialogDismissButton
                ]
                :: buttons
            )
        ]


viewDialogContent : DialogContent -> Html Msg
viewDialogContent contentType =
    case contentType of
        Form title label submitMsg content ->
            viewDialogModal
                title
                [ content
                , div [ class "mt-2 text-right" ]
                    [ button [ class "button mr-2", onClick DialogDismissingClicked ] [ text "Cancel" ]
                    , submitButton label submitMsg
                    ]
                ]

        DangerousQuestion question label confirmMsg ->
            viewQuestionModal
                question
                [ div [ class "mt-2 text-right last:mr-2" ]
                    [ button [ class "button mr-2", onClick DialogDismissingClicked ] [ text "Cancel" ]
                    , dangerousButton label confirmMsg
                    ]
                ]

        Question question label confirmMsg ->
            viewQuestionModal
                question
                [ div [ class "mt-2 text-right last:mr-2" ]
                    [ button [ class "button mr-2", onClick DialogDismissingClicked ] [ text "Cancel" ]
                    , primaryButton label confirmMsg
                    ]
                ]


viewDialog : Maybe Dialog -> Html Msg
viewDialog dialog =
    case dialog of
        Just model ->
            viewDialogContent (dialogContent model)

        Nothing ->
            div [ class "display-none" ] []


view : Time.Zone -> Model -> Html Msg
view timezone model =
    div []
        [ section [ class "bg-light rounded shadow my-8 p-8" ]
            [ header [ class "flex row mb-4" ]
                [ h2 [ class "text-xl font-bold mr-4 text-lighter" ] [ text "Manage VIPs" ]
                , normalButton "New VIP" GiveUserVipClicked
                , normalButton "Extend time for all" ExtendTimeForAllVipsClicked
                ]
            , div [ class "flex flex-wrap" ] (model.vips |> List.map (viewVip timezone))
            ]
        , section [ class "bg-light rounded shadow my-8 p-8" ]
            [ header [ class "flex row mb-4" ]
                [ h2 [ class "text-xl font-bold mr-4 text-lighter" ] [ text "Manage Members" ]
                , normalButton "New Member" DeclareUserAsMemberClicked
                ]
            , div [] [ viewMembers model.members ]
            ]
        , viewDialog model.dialog
        ]
