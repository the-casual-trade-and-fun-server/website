module Page.ServerManager.Types exposing
    ( CorrectTimeModel
    , CorrectTimeMsg(..)
    , DeclareUserAsMemberModel
    , DeclareUserAsMemberMsg(..)
    , Dialog(..)
    , ExtendTimeForAllMsg(..)
    , ExtendTimeForAllVipsModel
    , ExtendTimeModel
    , ExtendTimeMsg(..)
    , GiveUserVipModel
    , GiveUserVipMsg(..)
    , GroupedMembers
    , Intention(..)
    , Members
    , Model
    , Msg(..)
    , SetRankModel
    , SetRankMsg(..)
    , TransferTimeModel
    , TransferTimeMsg(..)
    )

import Backend
    exposing
        ( CorrectVipTimeError
        , ExtendTimeForAllVipsError
        , ExtendVipTimeError
        , GiveUserVipError
        , Member
        , Rank(..)
        , ServerManagerError
        , TransferTimeError
        , Vip
        )
import Time exposing (Posix)
import View.DropdownMenu exposing (MenuItem)


type alias GiveUserVipModel =
    { user : String
    , duration : Int
    }


type alias TransferTimeModel =
    { receiver : String
    , duration : Int
    }


type alias ExtendTimeModel =
    Int


type alias ExtendTimeForAllVipsModel =
    Int


type alias CorrectTimeModel =
    Int


type alias DeclareUserAsMemberModel =
    { user : String
    , rank : Rank
    }


type alias SetRankModel =
    Rank


type Dialog
    = GiveUserVip GiveUserVipModel
    | TransferTime Posix Vip TransferTimeModel
    | ConfirmPausing Vip
    | ConfirmResuming Vip
    | ExtendTime Vip ExtendTimeModel
    | ExtendTimeForAllVips ExtendTimeForAllVipsModel
    | CorrectTime Vip CorrectTimeModel
    | ConfirmRemoveDueToRuleBreaking Vip
    | DeclareUserAsMember DeclareUserAsMemberModel
    | SetRank Member SetRankModel
    | ConfirmRemoveMember Member


type GiveUserVipMsg
    = UserChanged String
    | DurationChanged String


type TransferTimeMsg
    = ReceiverChanged String
    | DurationToTransferChanged String


type ExtendTimeMsg
    = DurationToExtendChanged String


type ExtendTimeForAllMsg
    = DurationToExtendForAllChanged String


type CorrectTimeMsg
    = CorrectDurationChanged String


type DeclareUserAsMemberMsg
    = UserChanged_ String
    | RankSelected_ String


type SetRankMsg
    = RankSelected String


type Msg
    = Got_Vips (List Backend.Vip)
    | Failed_to_retrieve_Vips String
    | GiveUserVipClicked
    | GiveUserVipSubmitted
    | GiveUserVipMsg GiveUserVipMsg
    | UserReceivedVIP
    | FailedToGiveVIP (ServerManagerError GiveUserVipError)
    | TransferTimeClicked Vip
    | Got_Time_for_TransferTime Vip Posix
    | PauseVipClicked Vip
    | ConfirmPauseClicked Vip
    | VipPaused
    | FailedToPauseVip (ServerManagerError String)
    | ResumeVipClicked Vip
    | ConfirmResumeClicked Vip
    | VipResumed
    | FailedToResumeVip (ServerManagerError String)
    | ExtendTimeClicked Vip
    | CorrectTimeClicked Vip
    | DialogDismissingClicked
    | TransferTimeSubmitted
    | VipTransferredTime
    | FailedToTransferTime (ServerManagerError TransferTimeError)
    | TransferTimeMsg TransferTimeMsg
    | ExtendTimeSubmitted
    | VipTimeWasExtended String Int
    | FailedToExtendVipTime (ServerManagerError ExtendVipTimeError)
    | ExtendTimeMsg ExtendTimeMsg
    | ExtendTimeForAllVipsClicked
    | ExtendTimeForAllVipsSubmitted
    | TimeOfAllVipsWasExtended Int
    | FailedToExtendTimeForAllVips (ServerManagerError ExtendTimeForAllVipsError)
    | ExtendTimeForAllMsg ExtendTimeForAllMsg
    | CorrectTimeSubmitted
    | VipTimeWasCorrected String Int
    | FailedToCorrectVipTime (ServerManagerError CorrectVipTimeError)
    | CorrectTimeMsg CorrectTimeMsg
    | RemoveDueToRuleBreakingClicked Vip
    | RemoveDueToRuleBreakingConfirmed
    | VipWasRemovedDueToRuleBreaking String
    | FailedToRemoveDueToRuleBreaking (ServerManagerError String)
    | DeclareUserAsMemberClicked
    | DeclareUserAsMemberSubmitted
    | UserWasDeclaredAsMember
    | FailedToDeclareUserAsMember (ServerManagerError String)
    | DeclareUserAsMemberMsg DeclareUserAsMemberMsg
    | Got_Members (List Member)
    | Failed_to_retrieve_Members String
    | SetRankOfMemberClicked Member
    | SetRankOfMemberSubmitted
    | RankOfMemberChanged String Rank
    | FailedToSetRankOfMember (ServerManagerError String)
    | SetRankMsg SetRankMsg
    | RemoveMemberClicked Member
    | ConfirmRemoveMemberClicked
    | MemberRemoved String
    | FailedToRemoveMember (ServerManagerError String)


type Intention
    = NotifySuccess String
    | NotifyError String String
    | No_Intention


type alias Members =
    List ( Member, List (MenuItem Msg) )


type alias GroupedMembers =
    { owners : Maybe Members
    , admins : Maybe Members
    , moderators : Maybe Members
    , mappers : Maybe Members
    , permanentVips : Maybe Members
    }


type alias Model =
    { vips : List ( Vip, List (MenuItem Msg) )
    , members : GroupedMembers
    , dialog : Maybe Dialog
    }
