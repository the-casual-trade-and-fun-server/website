module Page.ServerManager.State exposing (init, update)

import Backend
    exposing
        ( Backend
        , CorrectVipTimeError
        , ExtendVipTimeError
        , GiveUserVipError
        , Member
        , ServerManagerError
        , Token
        , TransferTimeError
        , Vip
        )
import Page.ServerManager.Types exposing (CorrectTimeModel, CorrectTimeMsg(..), DeclareUserAsMemberModel, DeclareUserAsMemberMsg(..), Dialog(..), ExtendTimeForAllMsg(..), ExtendTimeForAllVipsModel, ExtendTimeModel, ExtendTimeMsg(..), GiveUserVipModel, GiveUserVipMsg(..), GroupedMembers, Intention(..), Members, Model, Msg(..), SetRankModel, SetRankMsg(..), TransferTimeModel, TransferTimeMsg(..))
import Task
import Time
import View.DropdownMenu exposing (MenuItem(..), MenuItemInfo)
import View.DurationInput exposing (tryParseDuration)


reloadVips : Backend -> Cmd Msg
reloadVips backend =
    Backend.retrieveAllVips backend Got_Vips Failed_to_retrieve_Vips


reloadMembers : Backend -> Cmd Msg
reloadMembers backend =
    Backend.retrieveAllMembers backend Got_Members Failed_to_retrieve_Members


vipPauseMenuItem : Vip -> MenuItemInfo Msg
vipPauseMenuItem vip =
    case vip.pausedSince of
        Just _ ->
            { icon = "fa-clock", label = "Resume VIP", action = ResumeVipClicked vip }

        Nothing ->
            { icon = "fa-clock", label = "Pause VIP", action = PauseVipClicked vip }


menuItemsOfVip : Vip -> List (MenuItem Msg)
menuItemsOfVip vip =
    [ Normal { icon = "fa-exchange-alt", label = "Transfer time", action = TransferTimeClicked vip }
    , Normal { icon = "fa-arrows-alt-h", label = "Extend time", action = ExtendTimeClicked vip }
    , Normal { icon = "fa-clock", label = "Correct time", action = CorrectTimeClicked vip }
    , vipPauseMenuItem vip |> Normal
    , Dangerous { icon = "fa-ban", label = "Remove due to rule breaking", action = RemoveDueToRuleBreakingClicked vip }
    ]


initVips : List Vip -> List ( Vip, List (MenuItem Msg) )
initVips vips =
    vips
        |> List.sortBy (\vip -> vip.name)
        |> List.map (\vip -> ( vip, menuItemsOfVip vip ))


withVips : List Vip -> Model -> Model
withVips vips model =
    { model | vips = initVips vips }


menuItemsOfMember : Member -> List (MenuItem Msg)
menuItemsOfMember member =
    [ Normal { icon = "fa-arrows-alt-h", label = "Set rank", action = SetRankOfMemberClicked member }
    , Dangerous { icon = "fa-trash", label = "Remove", action = RemoveMemberClicked member }
    ]


membersWithMenuItemsFor : Backend.Rank -> List Member -> Maybe Members
membersWithMenuItemsFor rank members =
    let
        filteredMembers =
            members |> List.filter (\member -> member.rank == rank)
    in
    if filteredMembers |> List.isEmpty then
        Nothing

    else
        filteredMembers
            |> List.sortBy (\member -> member.name)
            |> List.map (\member -> ( member, menuItemsOfMember member ))
            |> Just


groupMembers : List Member -> GroupedMembers
groupMembers members =
    { owners = members |> membersWithMenuItemsFor Backend.Owner
    , admins = members |> membersWithMenuItemsFor Backend.Admin
    , moderators = members |> membersWithMenuItemsFor Backend.Moderator
    , mappers = members |> membersWithMenuItemsFor Backend.Mapper
    , permanentVips = members |> membersWithMenuItemsFor Backend.PermanentVip
    }


allMembers : GroupedMembers -> List Member
allMembers members =
    [ members.owners
    , members.admins
    , members.moderators
    , members.mappers
    , members.permanentVips
    ]
        |> List.filterMap identity
        |> List.concat
        |> List.map (\( member, _ ) -> member)


withMembers : List Member -> Model -> Model
withMembers members model =
    { model | members = groupMembers members }


init : List Vip -> List Member -> Model
init vips members =
    { vips = initVips vips
    , members = groupMembers members
    , dialog = Nothing
    }


initGiveUserVip : GiveUserVipModel
initGiveUserVip =
    { user = ""
    , duration = 0
    }


initTransferTime : TransferTimeModel
initTransferTime =
    { receiver = ""
    , duration = 0
    }


initExtendTime : ExtendTimeModel
initExtendTime =
    0


initExtendTimeForAllVips : ExtendTimeForAllVipsModel
initExtendTimeForAllVips =
    0


initCorrectTime : CorrectTimeModel
initCorrectTime =
    0


initDeclareUserAsMember : DeclareUserAsMemberModel
initDeclareUserAsMember =
    { user = ""
    , rank = Backend.Moderator
    }


initSetRank : Backend.Rank -> SetRankModel
initSetRank currentRank =
    if currentRank == Backend.Mapper then
        Backend.PermanentVip

    else
        Backend.Mapper


giveUserVipErrorAsMsg : GiveUserVipError -> String
giveUserVipErrorAsMsg err =
    case err of
        Backend.AlreadyVip ->
            "The user already received VIP"

        Backend.CurrentlyPaused ->
            "The user already received VIP, but is currently paused"

        Backend.IsRuleBreaker ->
            "The user broke a rule once, so he can't receive VIP again"

        Backend.IsMember ->
            "The user is a team member. Team member can't get VIP"

        Backend.UnknownGiveUserVipError info ->
            "Something unexpected happend: " ++ info


transferTimeErrorAsMsg : TransferTimeError -> String
transferTimeErrorAsMsg err =
    case err of
        Backend.DurationIsNotAvailable ->
            "The VIP doen'st have the given duration to transfer"

        Backend.VIPIsExpired ->
            "The VIP is expired and therfore have no duration to transfer"

        Backend.UnknownTransferTimeError info ->
            "Something unexpected happend: " ++ info


serverManagerErrorAsMsg : (err -> String) -> ServerManagerError err -> String
serverManagerErrorAsMsg asMsg err =
    case err of
        Backend.Offline ->
            "It seems that you are offline. Please check your internet connection"

        Backend.ServerOffline ->
            "The server manager is not online. Please retry later"

        Backend.Failed innerErr ->
            asMsg innerErr

        Backend.InternalError info ->
            "Internal Error: " ++ info


extendVipTimeErrorAsMsg : ExtendVipTimeError -> String
extendVipTimeErrorAsMsg err =
    case err of
        Backend.Extend_VipIsExpired ->
            "The VIP is expired in the meantime. Give the user VIP if you want to grant VIP access again."

        Backend.UnknownExtendVipTimeError info ->
            "Something unexpected happend: " ++ info


extendTimeForAllVipsErrorAsMsg : Backend.ExtendTimeForAllVipsError -> String
extendTimeForAllVipsErrorAsMsg err =
    case err of
        Backend.DurationOfZero ->
            "The given duration should be greater than 0"

        Backend.UnknownExtendTimeForAllVipsError info ->
            "Something unexpected happend: " ++ info


correctVipTimeErrorAsMsg : CorrectVipTimeError -> String
correctVipTimeErrorAsMsg err =
    case err of
        Backend.Correct_VipIsExpired ->
            "The VIP is expired in the meantime. Give the user VIP if you want to grant VIP access again."

        Backend.UnknownCorrectVipTimeError info ->
            "Something unexpected happend: " ++ info


updateGiveUserVip : GiveUserVipModel -> GiveUserVipMsg -> GiveUserVipModel
updateGiveUserVip model msg =
    case msg of
        UserChanged user ->
            { model | user = user }

        DurationChanged input ->
            input
                |> tryParseDuration
                |> Maybe.map (\duration -> { model | duration = duration })
                |> Maybe.withDefault model


updateTransferTime : TransferTimeModel -> TransferTimeMsg -> TransferTimeModel
updateTransferTime model msg =
    case msg of
        ReceiverChanged receiver ->
            { model | receiver = receiver }

        DurationToTransferChanged input ->
            input
                |> tryParseDuration
                |> Maybe.map (\duration -> { model | duration = duration })
                |> Maybe.withDefault model


updateExtendTime : ExtendTimeModel -> ExtendTimeMsg -> ExtendTimeModel
updateExtendTime model msg =
    case msg of
        DurationToExtendChanged input ->
            input
                |> tryParseDuration
                |> Maybe.withDefault model


updateExtendTimeForAllVips : ExtendTimeForAllVipsModel -> ExtendTimeForAllMsg -> ExtendTimeForAllVipsModel
updateExtendTimeForAllVips model msg =
    case msg of
        DurationToExtendForAllChanged input ->
            input
                |> tryParseDuration
                |> Maybe.withDefault model


updateCorrectTime : CorrectTimeModel -> CorrectTimeMsg -> CorrectTimeModel
updateCorrectTime model msg =
    case msg of
        CorrectDurationChanged input ->
            input
                |> tryParseDuration
                |> Maybe.withDefault model


updateDeclareUserAsMember : DeclareUserAsMemberModel -> DeclareUserAsMemberMsg -> DeclareUserAsMemberModel
updateDeclareUserAsMember model msg =
    case msg of
        UserChanged_ user ->
            { model | user = user }

        RankSelected_ input ->
            Backend.parseRank input
                |> Maybe.map (\rank -> { model | rank = rank })
                |> Maybe.withDefault model


updateSetRank : SetRankModel -> SetRankMsg -> SetRankModel
updateSetRank model msg =
    case msg of
        RankSelected input ->
            Backend.parseRank input
                |> Maybe.withDefault model


giveUserVip : Backend -> Token -> String -> Int -> Cmd Msg
giveUserVip backend token user duration =
    Backend.giveUserVIP user duration backend token UserReceivedVIP FailedToGiveVIP


pauseVip : Backend -> Token -> String -> Cmd Msg
pauseVip backend token vip =
    Backend.pauseVip vip backend token VipPaused FailedToPauseVip


resumeVip : Backend -> Token -> String -> Cmd Msg
resumeVip backend token vip =
    Backend.resumeVip vip backend token VipResumed FailedToResumeVip


transferTimeOfVip : Backend -> Token -> String -> String -> Int -> Cmd Msg
transferTimeOfVip backend token sender receiver duration =
    Backend.transferTime sender receiver duration backend token VipTransferredTime FailedToTransferTime


extendTimeOfVip : Backend -> Token -> String -> Int -> Cmd Msg
extendTimeOfVip backend token vip duration =
    Backend.extendTime vip duration backend token (VipTimeWasExtended vip duration) FailedToExtendVipTime


extendTimeForAllVipsBy : Backend -> Token -> Int -> Cmd Msg
extendTimeForAllVipsBy backend token duration =
    Backend.extendTimeForAllVips duration backend token (TimeOfAllVipsWasExtended duration) FailedToExtendTimeForAllVips


correctTimeOfVip : Backend -> Token -> String -> Int -> Cmd Msg
correctTimeOfVip backend token vip duration =
    Backend.correctTime vip duration backend token (VipTimeWasCorrected vip duration) FailedToCorrectVipTime


removeDueToRuleBreaking : Backend -> Token -> String -> Cmd Msg
removeDueToRuleBreaking backend token vip =
    Backend.removeDueToRuleBreaking vip backend token (VipWasRemovedDueToRuleBreaking vip) FailedToRemoveDueToRuleBreaking


declareUserAsMember : Backend -> Token -> String -> Backend.Rank -> Cmd Msg
declareUserAsMember backend token user rank =
    Backend.declareUserAsMember user rank backend token UserWasDeclaredAsMember FailedToDeclareUserAsMember


setRankOfMember : Backend -> Token -> String -> Backend.Rank -> Cmd Msg
setRankOfMember backend token member rank =
    Backend.setRankOfMember member rank backend token (RankOfMemberChanged member rank) FailedToSetRankOfMember


removeMember : Backend -> Token -> String -> Cmd Msg
removeMember backend token member =
    Backend.removeMember member backend token (MemberRemoved member) FailedToRemoveMember


withUpdatedVipDuration : String -> (Int -> Int) -> Model -> Model
withUpdatedVipDuration vipId updater model =
    let
        vips =
            model.vips
                |> List.map
                    (\( vip, _ ) ->
                        if vip.id == vipId then
                            { vip | duration = updater vip.duration }

                        else
                            vip
                    )
    in
    model |> withVips vips


withAdditionalDurationForAllVips : Int -> Model -> Model
withAdditionalDurationForAllVips duration model =
    let
        vips =
            model.vips
                |> List.map (\( vip, _ ) -> { vip | duration = vip.duration + duration })
    in
    model |> withVips vips


durationAsMinutes : Int -> Int
durationAsMinutes duration =
    duration * 1440


withoutVip : String -> Model -> Model
withoutVip vipId model =
    let
        vips =
            model.vips
                |> List.filter (\( vip, _ ) -> vip.id /= vipId)
    in
    { model | vips = vips }


withNewRank : String -> Backend.Rank -> Member -> Member
withNewRank memberId rank member =
    if member.id == memberId then
        { member | rank = rank }

    else
        member


memberWithNewRank : String -> Backend.Rank -> GroupedMembers -> GroupedMembers
memberWithNewRank memberId rank members =
    members
        |> allMembers
        |> List.map (withNewRank memberId rank)
        |> groupMembers


membersWithout : String -> GroupedMembers -> GroupedMembers
membersWithout memberId members =
    members
        |> allMembers
        |> List.filter (\member -> member.id /= memberId)
        |> groupMembers


withoutDialog : Model -> Model
withoutDialog model =
    { model | dialog = Nothing }


update : Backend -> Token -> Model -> Msg -> ( Model, Cmd Msg, Intention )
update backend token model msg =
    case msg of
        Got_Vips vips ->
            ( model |> withVips vips |> withoutDialog, Cmd.none, No_Intention )

        Failed_to_retrieve_Vips error ->
            ( model, Cmd.none, NotifyError "Can't retrieve VIPs" error )

        GiveUserVipClicked ->
            ( { model | dialog = Just (GiveUserVip initGiveUserVip) }, Cmd.none, No_Intention )

        GiveUserVipSubmitted ->
            case model.dialog of
                Just (GiveUserVip innerModel) ->
                    ( model, giveUserVip backend token innerModel.user innerModel.duration, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        GiveUserVipMsg innerMsg ->
            case model.dialog of
                Just (GiveUserVip innerModel) ->
                    ( { model | dialog = Just (GiveUserVip (updateGiveUserVip innerModel innerMsg)) }, Cmd.none, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        UserReceivedVIP ->
            ( model, reloadVips backend, NotifySuccess "User received VIP" )

        FailedToGiveVIP error ->
            ( model, Cmd.none, NotifyError "Can't give user VIP" (serverManagerErrorAsMsg giveUserVipErrorAsMsg error) )

        TransferTimeClicked vip ->
            ( model, Task.perform (Got_Time_for_TransferTime vip) Time.now, No_Intention )

        Got_Time_for_TransferTime vip time ->
            ( { model | dialog = Just (TransferTime time vip initTransferTime) }, Cmd.none, No_Intention )

        PauseVipClicked vip ->
            ( { model | dialog = Just (ConfirmPausing vip) }, Cmd.none, No_Intention )

        ConfirmPauseClicked vip ->
            ( model, pauseVip backend token vip.id, No_Intention )

        VipPaused ->
            ( model, reloadVips backend, NotifySuccess "VIP paused" )

        FailedToPauseVip error ->
            ( model, Cmd.none, NotifyError "Can't pause VIP" (serverManagerErrorAsMsg (\i -> i) error) )

        ResumeVipClicked vip ->
            ( { model | dialog = Just (ConfirmResuming vip) }, Cmd.none, No_Intention )

        ConfirmResumeClicked vip ->
            ( model, resumeVip backend token vip.id, No_Intention )

        VipResumed ->
            ( model, reloadVips backend, NotifySuccess "VIP resumed" )

        FailedToResumeVip error ->
            ( model, Cmd.none, NotifyError "Can't resume VIP" (serverManagerErrorAsMsg (\i -> i) error) )

        ExtendTimeClicked vip ->
            ( { model | dialog = Just (ExtendTime vip initExtendTime) }, Cmd.none, No_Intention )

        CorrectTimeClicked vip ->
            ( { model | dialog = Just (CorrectTime vip initCorrectTime) }, Cmd.none, No_Intention )

        DialogDismissingClicked ->
            ( model |> withoutDialog, Cmd.none, No_Intention )

        TransferTimeSubmitted ->
            case model.dialog of
                Just (TransferTime _ vip m) ->
                    ( model, transferTimeOfVip backend token vip.id m.receiver m.duration, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        VipTransferredTime ->
            ( model, reloadVips backend, NotifySuccess "Time was transferred" )

        FailedToTransferTime error ->
            ( model, Cmd.none, NotifyError "Failed to transfer time" (serverManagerErrorAsMsg transferTimeErrorAsMsg error) )

        TransferTimeMsg innermsg ->
            case model.dialog of
                Just (TransferTime time vip m) ->
                    ( { model | dialog = Just (TransferTime time vip (updateTransferTime m innermsg)) }, Cmd.none, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        ExtendTimeMsg innermsg ->
            case model.dialog of
                Just (ExtendTime vip m) ->
                    ( { model | dialog = Just (ExtendTime vip (updateExtendTime m innermsg)) }, Cmd.none, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        ExtendTimeSubmitted ->
            case model.dialog of
                Just (ExtendTime vip duration) ->
                    ( model, extendTimeOfVip backend token vip.id duration, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        VipTimeWasExtended vip extraDuration ->
            ( model |> withUpdatedVipDuration vip ((+) (durationAsMinutes extraDuration)) |> withoutDialog, Cmd.none, NotifySuccess "Time was extended" )

        FailedToExtendVipTime error ->
            ( model, Cmd.none, NotifyError "Failed to extend time" (serverManagerErrorAsMsg extendVipTimeErrorAsMsg error) )

        ExtendTimeForAllVipsClicked ->
            ( { model | dialog = Just (ExtendTimeForAllVips initExtendTimeForAllVips) }, Cmd.none, No_Intention )

        ExtendTimeForAllVipsSubmitted ->
            case model.dialog of
                Just (ExtendTimeForAllVips duration) ->
                    ( model, extendTimeForAllVipsBy backend token duration, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        TimeOfAllVipsWasExtended duration ->
            ( model |> withAdditionalDurationForAllVips (durationAsMinutes duration) |> withoutDialog, Cmd.none, NotifySuccess "Time of all VIPs was extended" )

        FailedToExtendTimeForAllVips error ->
            ( model, Cmd.none, NotifyError "Failed to extend time for all VIPs" (serverManagerErrorAsMsg extendTimeForAllVipsErrorAsMsg error) )

        ExtendTimeForAllMsg innerMsg ->
            case model.dialog of
                Just (ExtendTimeForAllVips m) ->
                    ( { model | dialog = Just (ExtendTimeForAllVips (updateExtendTimeForAllVips m innerMsg)) }
                    , Cmd.none
                    , No_Intention
                    )

                _ ->
                    ( model, Cmd.none, No_Intention )

        CorrectTimeSubmitted ->
            case model.dialog of
                Just (CorrectTime vip duration) ->
                    ( model, correctTimeOfVip backend token vip.id duration, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        VipTimeWasCorrected vip newDuration ->
            ( model |> withUpdatedVipDuration vip (\_ -> durationAsMinutes newDuration) |> withoutDialog, Cmd.none, NotifySuccess "Time of VIP was corrected" )

        FailedToCorrectVipTime error ->
            ( model, Cmd.none, NotifyError "Failed to correct time" (serverManagerErrorAsMsg correctVipTimeErrorAsMsg error) )

        CorrectTimeMsg innermsg ->
            case model.dialog of
                Just (CorrectTime vip m) ->
                    ( { model | dialog = Just (CorrectTime vip (updateCorrectTime m innermsg)) }, Cmd.none, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        RemoveDueToRuleBreakingClicked vip ->
            ( { model | dialog = Just (ConfirmRemoveDueToRuleBreaking vip) }, Cmd.none, No_Intention )

        RemoveDueToRuleBreakingConfirmed ->
            case model.dialog of
                Just (ConfirmRemoveDueToRuleBreaking vip) ->
                    ( model, removeDueToRuleBreaking backend token vip.id, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        VipWasRemovedDueToRuleBreaking vip ->
            ( model |> withoutVip vip |> withoutDialog, Cmd.none, NotifySuccess "VIP was removed due to rule breaking" )

        FailedToRemoveDueToRuleBreaking error ->
            ( model, Cmd.none, NotifyError "Failed to remove VIP" (serverManagerErrorAsMsg identity error) )

        DeclareUserAsMemberClicked ->
            ( { model | dialog = Just (DeclareUserAsMember initDeclareUserAsMember) }
            , Cmd.none
            , No_Intention
            )

        DeclareUserAsMemberSubmitted ->
            case model.dialog of
                Just (DeclareUserAsMember innerModel) ->
                    ( model, declareUserAsMember backend token innerModel.user innerModel.rank, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        UserWasDeclaredAsMember ->
            ( model |> withoutDialog, reloadMembers backend, NotifySuccess "New member was added" )

        FailedToDeclareUserAsMember error ->
            ( model, Cmd.none, NotifyError "Failed to add member" (serverManagerErrorAsMsg identity error) )

        DeclareUserAsMemberMsg innerMsg ->
            case model.dialog of
                Just (DeclareUserAsMember innerModel) ->
                    ( { model | dialog = Just (DeclareUserAsMember (updateDeclareUserAsMember innerModel innerMsg)) }
                    , Cmd.none
                    , No_Intention
                    )

                _ ->
                    ( model, Cmd.none, No_Intention )

        Got_Members members ->
            ( model |> withMembers members, Cmd.none, No_Intention )

        Failed_to_retrieve_Members error ->
            ( model, Cmd.none, NotifyError "Can't reload members" error )

        SetRankOfMemberClicked member ->
            ( { model | dialog = Just (SetRank member (initSetRank member.rank)) }
            , Cmd.none
            , No_Intention
            )

        SetRankOfMemberSubmitted ->
            case model.dialog of
                Just (SetRank member rank) ->
                    ( model, setRankOfMember backend token member.id rank, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        RankOfMemberChanged member newRank ->
            ( { model | members = model.members |> memberWithNewRank member newRank, dialog = Nothing }, Cmd.none, NotifySuccess "Rank of member changed" )

        FailedToSetRankOfMember error ->
            ( { model | dialog = Nothing }, Cmd.none, NotifyError "Failed to set rank" (serverManagerErrorAsMsg identity error) )

        SetRankMsg innerMsg ->
            case model.dialog of
                Just (SetRank member m) ->
                    ( { model | dialog = Just (SetRank member (updateSetRank m innerMsg)) }, Cmd.none, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        RemoveMemberClicked member ->
            ( { model | dialog = Just (ConfirmRemoveMember member) }, Cmd.none, No_Intention )

        ConfirmRemoveMemberClicked ->
            case model.dialog of
                Just (ConfirmRemoveMember member) ->
                    ( model, removeMember backend token member.id, No_Intention )

                _ ->
                    ( model, Cmd.none, No_Intention )

        MemberRemoved member ->
            ( { model | members = model.members |> membersWithout member } |> withoutDialog, Cmd.none, NotifySuccess "Member removed" )

        FailedToRemoveMember error ->
            ( model |> withoutDialog, Cmd.none, NotifyError "Failed to remove member" (serverManagerErrorAsMsg identity error) )
