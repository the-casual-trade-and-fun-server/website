module Page.Settings.View exposing (view)

import Html exposing (Html, div, h2, i, input, li, span, text, ul)
import Html.Attributes exposing (class, type_)
import Html.Events exposing (onClick)
import MaybeExtra
import Page.Settings.CustomChatColors as CustomChatColors
import Page.Settings.Types exposing (AudioPlayer, Joinsound, Model(..), Msg(..), Settings, SettingsMsg(..))
import View.Button exposing (normalButton, normalSubmitButton)


viewActiveJoinsound : Maybe Joinsound -> Html SettingsMsg
viewActiveJoinsound active =
    case active of
        Just sound ->
            div [ class "bg-neutral-lightest p-2 rounded-t-lg border border-neutral" ]
                [ span [ class "mr-2" ]
                    [ span [ class "text-lighter" ] [ text "Currently used " ]
                    , text sound.name
                    ]
                , normalButton "Don't use joinsound" UseNoJoinsoundClicked
                ]

        Nothing ->
            div [ class "bg-neutral-lightest p-2 rounded-t-lg border border-neutral" ]
                [ text "You don't have a joinsound yet" ]


volumeClassFor : Float -> String
volumeClassFor volume =
    if volume == 0.0 then
        "fa-volume-off"

    else if volume < 0.5 then
        "fa-volume-down"

    else
        "fa-volume-up"


viewVolumeSlider : AudioPlayer -> Html SettingsMsg
viewVolumeSlider player =
    div [ class "flex justify-baseline" ]
        [ i [ class ("w-4 text-neutral fa " ++ volumeClassFor player.volume) ] []
        , input
            [ type_ "range"
            , Html.Attributes.max "1.0"
            , Html.Attributes.min "0.0"
            , Html.Attributes.step "0.1"
            , Html.Attributes.value (String.fromFloat player.volume)
            , Html.Events.onInput VolumeChanged
            ]
            []
        ]


viewSoundControl : Maybe Joinsound -> AudioPlayer -> Html SettingsMsg
viewSoundControl selectedSound player =
    div [ class "flex flex-row mb-2 mt-8 items-baseline" ] <|
        case selectedSound of
            Just sound ->
                [ if not player.playing then
                    i [ class "mr-4 cursor-pointer text-darker hover:text-neutral-dark fa fa-play", onClick (PlaySoundClicked sound) ] []

                  else
                    i [ class "mr-4 cursor-pointer text-darker hover:text-neutral-dark fa fa-pause", onClick PauseSoundClicked ] []
                , viewVolumeSlider player
                , div [ class "mx-2" ] [ normalSubmitButton "Use" (selectedSound |> MaybeExtra.filter (.used >> not) |> Maybe.map (\_ -> UseJoinsoundClicked)) ]
                , text sound.name
                ]

            Nothing ->
                [ i [ class "mr-4 fa fa-play fa-disabled text-lighter" ] []
                , viewVolumeSlider player
                ]


viewJoinsound : Maybe String -> Maybe Joinsound -> Joinsound -> Html SettingsMsg
viewJoinsound activeSound selectedSound sound =
    li
        [ class
            ("py-4 px-2 cursor-pointer "
                ++ (if selectedSound == Just sound then
                        "bg-neutral-lighter font-bold"

                    else
                        "bg-neutral-lightest"
                   )
            )
        , onClick (JoinsoundClicked sound)
        ]
        [ text sound.name
        , if activeSound == Just sound.previewUrl then
            div [ class "rounded-full p-1 inline bg-primary-lighter text-primary-darker ml-2 text-xs" ] [ text "used by you" ]

          else if sound.used then
            div [ class "rounded-full p-1 inline bg-secondary-lightest text-secondary-dark ml-2 text-xs" ] [ text "used" ]

          else
            text ""
        ]


viewJoinsounds : Maybe String -> Maybe Joinsound -> List Joinsound -> Html SettingsMsg
viewJoinsounds active selected sounds =
    ul [ class "h-56 overflow-y-scroll rounded-b-lg shadow-inner border border-neutral" ]
        (sounds |> List.map (viewJoinsound active selected))


activeJoinsound : Settings -> Maybe Joinsound
activeJoinsound settings =
    settings.joinsounds
        |> List.filter (\s -> Just s.previewUrl == settings.activeJoinsound)
        |> List.head


viewSettings : Settings -> Html SettingsMsg
viewSettings settings =
    div [ class "flex flex-wrap" ]
        [ div [ class "w-full max-w-lg bg-light rounded shadow p-4 mx-8 mt-8" ]
            [ h2 [ class "text-neutral text-xl font-bold mb-4" ] [ text "Joinsound" ]
            , viewSoundControl settings.selectedSound settings.player
            , viewActiveJoinsound (activeJoinsound settings)
            , viewJoinsounds settings.activeJoinsound settings.selectedSound settings.joinsounds
            ]
        , div [ class "h-48 w-full max-w-lg bg-light rounded shadow p-4 m-8" ]
            [ h2 [ class "text-neutral text-xl font-bold mb-8" ] [ text "Custom Chat Color" ]
            , CustomChatColors.view settings.customChatColors |> Html.map CustomChatColorsMsg
            ]
        ]


view : Model -> Html Msg
view model =
    case model of
        Loading ->
            i [ class "fa fa-spinner" ] []

        Loaded settings ->
            viewSettings settings |> Html.map SettingsMsg

        Failed err ->
            div [] [ text ("Oh no: " ++ err) ]
