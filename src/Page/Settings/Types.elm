module Page.Settings.Types exposing (AudioPlayer, Intention(..), Joinsound, Model(..), Msg(..), Settings, SettingsIntention(..), SettingsMsg(..))

import Backend
import Page.Settings.CustomChatColors as CustomChatColors


type alias Joinsound =
    { name : String
    , previewUrl : String
    , used : Bool
    }


type alias AudioPlayer =
    { playing : Bool
    , volume : Float
    }


type alias Settings =
    { activeJoinsound : Maybe String
    , selectedSound : Maybe Joinsound
    , joinsounds : List Joinsound
    , player : AudioPlayer
    , customChatColors : CustomChatColors.Model
    }


type Model
    = Loading
    | Loaded Settings
    | Failed String


type Intention
    = NotifySuccess String
    | NotifyError String String


type SettingsMsg
    = JoinsoundClicked Joinsound
    | PlaySoundClicked Joinsound
    | PauseSoundClicked
    | VolumeChanged String
    | SoundEnded String
    | UseJoinsoundClicked
    | UseNoJoinsoundClicked
    | CustomChatColorsMsg CustomChatColors.Msg


type SettingsIntention
    = SetJoinsound String
    | UseNoJoinsound
    | PlaySound String
    | PauseSound
    | SetVolume Float
    | UseCustomChatColors CustomChatColors.CustomChatColors


type Msg
    = SettingsLoaded Backend.InitialDataForSettings
    | FailedToLoadSettings String
    | JoinsoundWasChanged String
    | FailedToSetJoinsound (Backend.ServerManagerError String)
    | JoinsoundWasRemoved
    | FailedToUseNoJoinsound (Backend.ServerManagerError String)
    | CustomChatColorsHaveChanged CustomChatColors.CustomChatColors
    | FailedToSetCustomChatColors (Backend.ServerManagerError String)
    | SettingsMsg SettingsMsg
