module Page.Settings.CustomChatColors exposing (CustomChatColors, Intention(..), Model, Msg, asBackendColors, init, update, view, withCurrentlyUsed)

import Backend
import Html exposing (Html, div, input, span, text)
import Html.Attributes exposing (class, style, type_, value)
import Html.Events exposing (onClick, onInput)
import View.Button exposing (normalButton, normalSubmitButton, submitButton)


type ColorToEdit
    = Tag
    | Name
    | Chat


type alias CustomChatColors =
    { tag : Maybe String
    , name : Maybe String
    , chat : Maybe String
    }


type alias Model =
    { currentlyUsed : CustomChatColors
    , current : CustomChatColors
    , colorToEdit : Maybe ColorToEdit
    }


type Intention
    = UseColors CustomChatColors


type Msg
    = ColorClicked ColorToEdit
    | ColorChanged ColorToEdit String
    | UseDefaultColorClicked ColorToEdit
    | CancelColorSelectionClicked
    | UseCustomChatColorsClicked
    | CancelCustomChatColorsClicked


init : Backend.CustomChatColors -> Model
init colors =
    let
        customColors =
            { tag = colors.tag
            , name = colors.name
            , chat = colors.chat
            }
    in
    { currentlyUsed = customColors
    , current = customColors
    , colorToEdit = Nothing
    }


asBackendColors : CustomChatColors -> Backend.CustomChatColors
asBackendColors colors =
    { tag = colors.tag
    , name = colors.name
    , chat = colors.chat
    }


withoutIntention : Model -> ( Model, Maybe Intention )
withoutIntention model =
    ( model, Nothing )


withColorToEdit : ColorToEdit -> Model -> Model
withColorToEdit color settings =
    { settings | colorToEdit = Just color }


withoutColorToEdit : Model -> Model
withoutColorToEdit settings =
    { settings | colorToEdit = Nothing }


withTagColor : String -> CustomChatColors -> CustomChatColors
withTagColor color colors =
    { colors | tag = Just color }


withoutTagColor : CustomChatColors -> CustomChatColors
withoutTagColor colors =
    { colors | tag = Nothing }


withNameColor : String -> CustomChatColors -> CustomChatColors
withNameColor color colors =
    { colors | name = Just color }


withoutNameColor : CustomChatColors -> CustomChatColors
withoutNameColor colors =
    { colors | name = Nothing }


withChatColor : String -> CustomChatColors -> CustomChatColors
withChatColor color colors =
    { colors | chat = Just color }


withoutChatColor : CustomChatColors -> CustomChatColors
withoutChatColor colors =
    { colors | chat = Nothing }


withUpdatedCurrentColors : (CustomChatColors -> CustomChatColors) -> Model -> Model
withUpdatedCurrentColors updater settings =
    { settings | current = updater settings.current }


withOldColors : Model -> Model
withOldColors settings =
    { settings | current = settings.currentlyUsed }


withCurrentlyUsed : CustomChatColors -> Model -> Model
withCurrentlyUsed colors settings =
    { settings | current = colors, currentlyUsed = colors }


update : Msg -> Model -> ( Model, Maybe Intention )
update msg model =
    case msg of
        ColorClicked color ->
            model
                |> withColorToEdit color
                |> withoutIntention

        ColorChanged colorToEdit color ->
            case colorToEdit of
                Tag ->
                    model
                        |> withUpdatedCurrentColors (withTagColor color)
                        |> withoutColorToEdit
                        |> withoutIntention

                Name ->
                    model
                        |> withUpdatedCurrentColors (withNameColor color)
                        |> withoutColorToEdit
                        |> withoutIntention

                Chat ->
                    model
                        |> withUpdatedCurrentColors (withChatColor color)
                        |> withoutColorToEdit
                        |> withoutIntention

        UseDefaultColorClicked colorToEdit ->
            case colorToEdit of
                Tag ->
                    model
                        |> withUpdatedCurrentColors withoutTagColor
                        |> withoutColorToEdit
                        |> withoutIntention

                Name ->
                    model
                        |> withUpdatedCurrentColors withoutNameColor
                        |> withoutColorToEdit
                        |> withoutIntention

                Chat ->
                    model
                        |> withUpdatedCurrentColors withoutChatColor
                        |> withoutColorToEdit
                        |> withoutIntention

        CancelColorSelectionClicked ->
            model
                |> withoutColorToEdit
                |> withoutIntention

        UseCustomChatColorsClicked ->
            if model.current /= model.currentlyUsed then
                ( model, Just (UseColors model.current) )

            else
                model |> withoutIntention

        CancelCustomChatColorsClicked ->
            model
                |> withOldColors
                |> withoutIntention


viewColorItem : ColorToEdit -> Maybe String -> String -> Html Msg
viewColorItem colorToEdit currentColor color =
    div
        [ class "h-12 w-12 m-2 flex justify-center items-center rounded-full border border-neutral cursor-pointer"
        , style "background-color" color
        , onClick (ColorChanged colorToEdit color)
        ]
        (if currentColor == Just color then
            [ span [ class "contrast-text" ] [ text "✓" ] ]

         else
            []
        )


viewDefaultColorItem : ColorToEdit -> Html Msg
viewDefaultColorItem colorToEdit =
    div
        [ class "h-12 w-12 m-2 flex justify-center items-center rounded-full border border-neutral cursor-pointer"
        , style "background-color" "transparent"
        , onClick (UseDefaultColorClicked colorToEdit)
        ]
        [ text "D" ]


customColorItem : ColorToEdit -> String -> Html Msg
customColorItem colorToEdit currentColor =
    input
        [ type_ "color"
        , class "h-12 w-12 m-2 border border-neutral cursor-pointer"
        , value currentColor
        , onInput (ColorChanged colorToEdit)
        ]
        []


viewCustomChatColorPicker : ColorToEdit -> Maybe String -> Html Msg
viewCustomChatColorPicker colorToEdit currentColor =
    let
        colorItem =
            viewColorItem colorToEdit currentColor
    in
    div [ class "w-64" ]
        [ div [ class "flex flex-wrap" ]
            [ viewDefaultColorItem colorToEdit
            , customColorItem colorToEdit (currentColor |> Maybe.withDefault "#000000")
            , colorItem "#63B3ED"
            , colorItem "#667EEA"
            , colorItem "#2C5282"
            , colorItem "#E53E3E"
            , colorItem "#FBB6CE"
            , colorItem "#38B2AC"
            , colorItem "#9AE6B4"
            , colorItem "#38A169"
            , colorItem "#FEEBC8"
            , colorItem "#DD6B20"
            , colorItem "#F6E05E"
            , colorItem "#FEFCBF"
            , colorItem "#E9D8FD"
            , colorItem "#B794F4"
            , colorItem "#553C9A"
            , colorItem "#A9DDF2"
            , colorItem "#3BE482"
            , colorItem "#9E21AF"
            ]
        , div [ class "mt-4 text-center" ] [ normalButton "Cancel" CancelColorSelectionClicked ]
        ]


colorOrDefault : Maybe String -> String
colorOrDefault color =
    color |> Maybe.withDefault "#d6c8ad"


viewCustomColorChat : String -> Bool -> Msg -> ColorToEdit -> Maybe String -> Html Msg
viewCustomColorChat message editing clickMsg colorToEdit color =
    div [ class "relative" ]
        [ div
            [ style "top" "24px"
            , style "left" "24px"
            , class
                ("absolute inset-x-auto p-8 z-10 bg-neutral-lightest shadow-xl rounded-b rounded-tr"
                    ++ (if editing then
                            ""

                        else
                            " hidden"
                       )
                )
            ]
            [ viewCustomChatColorPicker colorToEdit color ]
        , span [ style "color" (color |> colorOrDefault), class "chat-preview", onClick clickMsg ] [ text message ]
        ]


view : Model -> Html Msg
view settings =
    div []
        [ div [ class "bg-neutral-lighter text-shadow my-4 p-2 rounded flex" ]
            [ settings.current.tag |> viewCustomColorChat "[TAG]" (settings.colorToEdit == Just Tag) (ColorClicked Tag) Tag
            , span [ class "ml-1" ] [ settings.current.name |> viewCustomColorChat "PlayerName" (settings.colorToEdit == Just Name) (ColorClicked Name) Name ]
            , settings.current.chat |> viewCustomColorChat ": Your Message!" (settings.colorToEdit == Just Chat) (ColorClicked Chat) Chat
            ]
        , normalSubmitButton "Cancel"
            (if settings.current /= settings.currentlyUsed then
                Just CancelCustomChatColorsClicked

             else
                Nothing
            )
        , submitButton "Use those colors"
            (if settings.current /= settings.currentlyUsed then
                Just UseCustomChatColorsClicked

             else
                Nothing
            )
        ]
