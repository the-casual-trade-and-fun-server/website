module Page.Settings.State exposing (init, subscriptions, update)

import Backend
import MaybeExtra
import Page.Settings.CustomChatColors as CustomChatColors
import Page.Settings.Types exposing (AudioPlayer, Intention(..), Joinsound, Model(..), Msg(..), Settings, SettingsIntention(..), SettingsMsg(..))
import SoundPlayer as Player


loadInitialData : Backend.Backend -> String -> Cmd Msg
loadInitialData backend user =
    Backend.retrieveInitialDataForSettings
        backend
        user
        SettingsLoaded
        FailedToLoadSettings


setJoinsound : Backend.Backend -> Backend.Token -> String -> String -> Cmd Msg
setJoinsound backend token user sound =
    Backend.setJoinsound
        user
        sound
        backend
        token
        (JoinsoundWasChanged sound)
        FailedToSetJoinsound


useNoJoinsound : Backend.Backend -> Backend.Token -> String -> Cmd Msg
useNoJoinsound backend token user =
    Backend.useNoJoinsound
        user
        backend
        token
        JoinsoundWasRemoved
        FailedToUseNoJoinsound


setCustomChatColors : Backend.Backend -> Backend.Token -> String -> CustomChatColors.CustomChatColors -> Cmd Msg
setCustomChatColors backend token user colors =
    Backend.setCustomChatColors
        user
        (colors |> CustomChatColors.asBackendColors)
        backend
        token
        (CustomChatColorsHaveChanged colors)
        FailedToSetCustomChatColors


init : Backend.Backend -> String -> ( Model, Cmd Msg )
init backend user =
    ( Loading, loadInitialData backend user )


asJoinsound : Backend.Joinsound -> Joinsound
asJoinsound sound =
    { name = sound.name
    , previewUrl = sound.previewUrl
    , used = sound.used
    }


initAudioPlayer : AudioPlayer
initAudioPlayer =
    { playing = False
    , volume = 0.5
    }


asSettings : Backend.InitialDataForSettings -> Settings
asSettings data =
    { activeJoinsound = data.joinsound
    , selectedSound = Nothing
    , player = initAudioPlayer
    , joinsounds =
        data.definedJoinsounds
            |> List.map asJoinsound
            |> List.sortBy .name
    , customChatColors = data.customChatColors |> CustomChatColors.init
    }


toggleUsed : Maybe String -> String -> List Joinsound -> List Joinsound
toggleUsed old sound sounds =
    sounds
        |> List.map
            (\s ->
                if s.previewUrl == sound then
                    { s | used = True }

                else if Just s.previewUrl == old then
                    { s | used = False }

                else
                    s
            )


withoutIntention : Settings -> ( Settings, Maybe SettingsIntention )
withoutIntention model =
    ( model, Nothing )


withActiveSound : String -> Settings -> Settings
withActiveSound sound model =
    { model
        | activeJoinsound = Just sound
        , joinsounds = toggleUsed model.activeJoinsound sound model.joinsounds
    }


withUnused : String -> List Joinsound -> List Joinsound
withUnused sound joinsounds =
    joinsounds
        |> List.map
            (\s ->
                if s.previewUrl == sound then
                    { s | used = False }

                else
                    s
            )


withoutActiveSound : Settings -> Settings
withoutActiveSound model =
    { model
        | activeJoinsound = Nothing
        , joinsounds =
            model.activeJoinsound
                |> Maybe.map (\sound -> model.joinsounds |> withUnused sound)
                |> Maybe.withDefault model.joinsounds
    }


withColors : CustomChatColors.CustomChatColors -> Settings -> Settings
withColors colors model =
    { model | customChatColors = model.customChatColors |> CustomChatColors.withCurrentlyUsed colors }


playing : AudioPlayer -> AudioPlayer
playing player =
    { player | playing = True }


paused : AudioPlayer -> AudioPlayer
paused player =
    { player | playing = False }


stopped : AudioPlayer -> AudioPlayer
stopped player =
    { player | playing = False }


withVolume : Float -> AudioPlayer -> AudioPlayer
withVolume volume player =
    { player | volume = volume }


translateColorsIntention : CustomChatColors.Intention -> SettingsIntention
translateColorsIntention intention =
    case intention of
        CustomChatColors.UseColors colors ->
            UseCustomChatColors colors


updateSettings : SettingsMsg -> Settings -> ( Settings, Maybe SettingsIntention )
updateSettings msg model =
    case msg of
        JoinsoundClicked sound ->
            { model | selectedSound = Just sound }
                |> withoutIntention

        PlaySoundClicked sound ->
            ( { model | player = playing model.player }, Just (PlaySound sound.previewUrl) )

        PauseSoundClicked ->
            ( { model | player = paused model.player }, Just PauseSound )

        VolumeChanged volumeInput ->
            case String.toFloat volumeInput of
                Just volume ->
                    ( { model | player = model.player |> withVolume volume }, Just (SetVolume volume) )

                Nothing ->
                    model |> withoutIntention

        SoundEnded _ ->
            { model | player = stopped model.player }
                |> withoutIntention

        UseJoinsoundClicked ->
            ( model
            , model.selectedSound
                |> MaybeExtra.filter (\s -> not s.used)
                |> Maybe.map (.previewUrl >> SetJoinsound)
            )

        UseNoJoinsoundClicked ->
            ( model
            , model.activeJoinsound
                |> Maybe.map (\_ -> UseNoJoinsound)
            )

        CustomChatColorsMsg innerMsg ->
            let
                ( newModel, intention ) =
                    CustomChatColors.update innerMsg model.customChatColors
            in
            ( { model | customChatColors = newModel }
            , intention |> Maybe.map translateColorsIntention
            )


performIntention : Backend.Backend -> Backend.Token -> String -> SettingsIntention -> Cmd Msg
performIntention backend token user intention =
    case intention of
        SetJoinsound sound ->
            setJoinsound backend token user sound

        UseNoJoinsound ->
            useNoJoinsound backend token user

        UseCustomChatColors colors ->
            setCustomChatColors backend token user colors

        PlaySound sound ->
            Player.playSound ("assets/joinsounds/" ++ sound)

        PauseSound ->
            Player.pauseSound ()

        SetVolume volume ->
            Player.setVolume volume


errorAsString : Backend.ServerManagerError String -> String
errorAsString error =
    case error of
        Backend.Offline ->
            "It seems that you are currently offline"

        Backend.ServerOffline ->
            "I can't contact the server. Please try again later"

        Backend.Failed err ->
            err

        Backend.InternalError err ->
            err


doForSettings : (Settings -> ( Settings, Cmd Msg, Maybe Intention )) -> Model -> ( Model, Cmd Msg, Maybe Intention )
doForSettings updater model =
    case model of
        Loaded settings ->
            let
                ( newSettings, cmd, intention ) =
                    updater settings
            in
            ( Loaded newSettings, cmd, intention )

        _ ->
            ( model, Cmd.none, Nothing )


update : Backend.Backend -> Backend.Token -> String -> Msg -> Model -> ( Model, Cmd Msg, Maybe Intention )
update backend token user msg model =
    case msg of
        SettingsLoaded data ->
            ( data |> asSettings |> Loaded, Cmd.none, Nothing )

        FailedToLoadSettings err ->
            ( Failed err, Cmd.none, Nothing )

        JoinsoundWasChanged sound ->
            model
                |> doForSettings (\settings -> ( settings |> withActiveSound sound, Cmd.none, Just (NotifySuccess "Joinsound was updated") ))

        FailedToSetJoinsound err ->
            ( model, Cmd.none, err |> errorAsString |> NotifyError "Can't set joinsound" |> Just )

        JoinsoundWasRemoved ->
            model
                |> doForSettings (\settings -> ( settings |> withoutActiveSound, Cmd.none, Just (NotifySuccess "Next time you will join the server without joinsound") ))

        FailedToUseNoJoinsound err ->
            ( model, Cmd.none, err |> errorAsString |> NotifyError "Can't set joinsound" |> Just )

        CustomChatColorsHaveChanged colors ->
            model
                |> doForSettings (\settings -> ( settings |> withColors colors, Cmd.none, Just (NotifySuccess "You will now use those colors") ))

        FailedToSetCustomChatColors err ->
            ( model, Cmd.none, err |> errorAsString |> NotifyError "I can't give you those fancy colors" |> Just )

        SettingsMsg subMsg ->
            model
                |> doForSettings
                    (\settings ->
                        let
                            ( newSettings, intention ) =
                                updateSettings subMsg settings
                        in
                        ( newSettings
                        , intention
                            |> Maybe.map (performIntention backend token user)
                            |> Maybe.withDefault Cmd.none
                        , Nothing
                        )
                    )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Loaded _ ->
            Player.soundEnded SoundEnded
                |> Sub.map SettingsMsg

        _ ->
            Sub.none
