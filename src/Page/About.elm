module Page.About exposing (view)

import Html exposing (Html, a, article, h1, h4, i, img, main_, p, section, span, text)
import Html.Attributes exposing (class, href, src)


viewLogo : Html msg
viewLogo =
    img [ class "mx-auto", src "/assets/logo-night.png" ] []


viewBackLink : Html msg
viewBackLink =
    a [ class "button bg-default ml-8 fixed", href "/" ] [ i [ class "fas fa-arrow-left mr-1" ] [], text "Back" ]


viewSection : String -> List (Html msg) -> Html msg
viewSection header content =
    section [ class "mt-4 px-4 clear-both" ] (List.append [ h4 [ class "font-bold uppercase text-neutral-light" ] [ text header ] ] content)


viewGeneralInfo : Html msg
viewGeneralInfo =
    viewSection "What is this server about?"
        [ p [ class "mb-2" ]
            [ text "Friendly environment, nostalgic flair, trading and bonus dux! We are working hard to provide a server for what we value the most. The main value of the server is "
            , span [ class "font-bold" ] [ text "having fun" ]
            , text ". You might now ask yourself \"Fun? What is that?\". It's that what you experience on the server! But jokes aside."
            ]
        , p [ class "mb-2" ]
            [ img [ src "/assets/maps/trade_jollyrogerbay_v3/thumbnail.jpg", class "border-2 border-neutral md:float-left mx-auto md:mx-0 md:mr-2 mt-1" ] []
            , text "Over the years we collected quite some awesome maps, each provides enough content to explore and interact with. Some provides nostalgia if you have played retro games. Others are just a place to play around. Go see the "
            , a [ class "link", href "/maps" ] [ text "full list of maps" ]
            , text " or join the server and check it out jourself."
            ]
        , p [ class "mb-2" ]
            [ img [ src "/assets/maps/oot3d_hyrule/thumbnail.jpg", class "border-2 border-neutral md:float-right mx-auto md:mx-0 md:ml-2 mt-1" ] []
            , text "Besides exploring or playing around with maps, we got a duck load of game additions for you. Want to decorate your sentry with a cool hat? No problem. Want to fly around with a jetpack? We got you!"
            , p [] [ text "All those extras will bring more joy to your stay on our server" ]
            ]
        ]


viewHowItAllStarted : Html msg
viewHowItAllStarted =
    viewSection "How it all started"
        [ p [] [ text "Started as a side project by Hannes so he could explore play around and explore " ]
        ]


viewAboutTheOwners : Html msg
viewAboutTheOwners =
    viewSection "About the owners"
        [ p [] [ text "Shadow_Man" ] ]


view : Html msg
view =
    main_ [ class "dark bg-background min-h-screen pt-8" ]
        [ viewBackLink
        , viewLogo
        , h1 [ class "font-bold text-xl text-center" ] [ text "The Casual Trade and Fun Server" ]
        , article [ class "mx-auto container" ]
            [ viewGeneralInfo
            , viewHowItAllStarted
            , viewAboutTheOwners
            ]
        ]
