module Utils exposing (withCommand, withCommands, withoutCommands)


withoutCommands : model -> ( model, Cmd msg )
withoutCommands model =
    ( model, Cmd.none )


withCommand : Cmd msg -> model -> ( model, Cmd msg )
withCommand cmd model =
    ( model, cmd )


withCommands : List (Cmd msg) -> model -> ( model, Cmd msg )
withCommands cmds model =
    ( model, Cmd.batch cmds )
