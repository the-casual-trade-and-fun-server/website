port module BrightnessStorage exposing (Brightness(..), brightnessClass, parseBrightness, rememberDarkmode, rememberLightmode)


type Brightness
    = Light
    | Dark


port set : String -> Cmd msg


parseBrightness : String -> Brightness
parseBrightness input =
    case input of
        "light" ->
            Light

        _ ->
            Dark


brightnessClass : Brightness -> String
brightnessClass brightness =
    case brightness of
        Light ->
            ""

        Dark ->
            "dark"


rememberDarkmode : Cmd msg
rememberDarkmode =
    set "dark"


rememberLightmode : Cmd msg
rememberLightmode =
    set "light"
