module View.DurationInput exposing (isDurationValid, isDurationValidWithLimit, tryParseDuration, view, viewWithLimit)

import Html exposing (Html, div, input, span, text)
import Html.Attributes exposing (class, type_, value)
import Html.Events exposing (onInput)


dayText : Int -> String
dayText days =
    if days == 1 then
        "day"

    else
        "days"


isDurationAbove : Int -> Int -> Bool
isDurationAbove limit duration =
    duration > limit


isDurationNotAbove : Int -> Int -> Bool
isDurationNotAbove limit duration =
    not (isDurationAbove limit duration)


isDurationValid : Int -> Bool
isDurationValid duration =
    duration > 0


isDurationValidWithLimit : Int -> Int -> Bool
isDurationValidWithLimit limit duration =
    isDurationValid duration && isDurationNotAbove limit duration


tryParseDuration : String -> Maybe Int
tryParseDuration input =
    if input |> String.isEmpty then
        Just 0

    else
        input |> String.toInt


validationClass : Int -> String
validationClass duration =
    if not (isDurationValid duration) then
        "border-dangerous"

    else
        ""


validationClassWithLimit : Int -> Int -> String
validationClassWithLimit limit duration =
    if not (isDurationValidWithLimit limit duration) then
        "border-dangerous"

    else
        ""


viewWithLimit : (String -> msg) -> Int -> Int -> Html msg
viewWithLimit changeMsg limit duration =
    div [ class "relative w-56" ]
        [ input
            [ class ("form-element w-full pr-32 border " ++ validationClassWithLimit limit duration)
            , type_ "number"
            , onInput changeMsg
            , value (String.fromInt duration)
            ]
            []
        , span [ class "absolute h-full w-32 text-left pl-1 pt-1 text-light leading-relaxed right-0" ]
            [ text (dayText duration)
            , span
                [ class
                    (if isDurationAbove limit duration then
                        "text-dangerous"

                     else
                        ""
                    )
                ]
                [ text " of "
                , span [ class "font-bold" ] [ text (String.fromInt limit) ]
                , text (" " ++ dayText limit)
                ]
            ]
        ]


view : (String -> msg) -> Int -> Html msg
view changeMsg duration =
    div [ class "relative w-56" ]
        [ input
            [ class ("form-element w-full pr-12 border " ++ validationClass duration)
            , type_ "number"
            , onInput changeMsg
            , value (String.fromInt duration)
            ]
            []
        , span [ class "absolute h-full w-12 text-left pl-1 pt-1 text-light leading-relaxed right-0" ] [ text (dayText duration) ]
        ]
