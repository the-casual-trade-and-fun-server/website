module View.DropdownMenu exposing (MenuItem(..), MenuItemInfo, dropdownMenu)

import Html exposing (Html, button, div, i, li, text, ul)
import Html.Attributes exposing (class)
import Html.Events exposing (onMouseDown)


type alias MenuItemInfo msg =
    { icon : String
    , label : String
    , action : msg
    }


type MenuItem msg
    = Normal (MenuItemInfo msg)
    | Dangerous (MenuItemInfo msg)


dropdownMenuItem : MenuItemInfo msg -> Html msg
dropdownMenuItem item =
    li [ class "dropdown-item", onMouseDown item.action ] [ i [ class ("mr-4 text-lighter fas " ++ item.icon) ] [], text item.label ]


dangerousDropdownMenuItem : MenuItemInfo msg -> Html msg
dangerousDropdownMenuItem item =
    li [ class "dropdown-item text-dangerous-light", onMouseDown item.action ] [ i [ class ("mr-4 text-dangerous-lighter fas " ++ item.icon) ] [], text item.label ]


menuItem : MenuItem msg -> Html msg
menuItem item =
    case item of
        Normal info ->
            dropdownMenuItem info

        Dangerous info ->
            dangerousDropdownMenuItem info


dropdownMenu : List (MenuItem msg) -> Html msg
dropdownMenu items =
    div [ class "dropdown inline" ]
        [ button [ class "icon text-light fas fa-ellipsis-v px-4" ] []
        , div [ class "dropdown-content" ] [ ul [] (items |> List.map menuItem) ]
        ]
