module View.Dialog exposing (error, success)

import Html exposing (Html, div, h4, i, span, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)


viewDialogDismissButton : msg -> Html msg
viewDialogDismissButton dismissMsg =
    i [ onClick dismissMsg, class "fas fa-times font-normal text-base ml-4 cursor-pointer" ] []


success : String -> msg -> Html msg
success info msg =
    div [ class "dialog success flex items-baseline justify-space-between" ]
        [ span [ class "font-bold" ] [ text info ]
        , viewDialogDismissButton msg
        ]


error : String -> String -> msg -> Html msg
error head info msg =
    div [ class "dialog error" ]
        [ h4 [ class "font-bold flex items-baseline justify-between" ]
            [ text head
            , viewDialogDismissButton msg
            ]
        , text info
        ]
