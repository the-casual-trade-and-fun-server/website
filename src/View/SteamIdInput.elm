module View.SteamIdInput exposing (isValid, view)

import Html exposing (Html, input)
import Html.Attributes exposing (class, type_, value)
import Html.Events exposing (onInput)


isValid : String -> Bool
isValid user =
    String.startsWith "STEAM_" user
        && (user
                |> String.split ":"
                |> List.filter (not << String.isEmpty)
                |> List.length
                |> (==) 3
           )


validationClass : String -> String
validationClass steamId =
    if not (isValid steamId) then
        "border-dangerous"

    else
        ""


view : (String -> msg) -> String -> Html msg
view changeMsg steamId =
    input
        [ class ("form-element border " ++ validationClass steamId)
        , type_ "text"
        , onInput changeMsg
        , value steamId
        ]
        []
