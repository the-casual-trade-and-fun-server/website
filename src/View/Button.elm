module View.Button exposing (dangerousButton, normalButton, normalSubmitButton, primaryButton, submitButton)

import Html exposing (Html, button, text)
import Html.Attributes exposing (class, disabled)
import Html.Events exposing (onClick)


submitButton : String -> Maybe msg -> Html msg
submitButton label clickMsg =
    let
        props =
            case clickMsg of
                Just msg ->
                    [ onClick msg ]

                Nothing ->
                    [ disabled True ]
    in
    button (class "button primary" :: props) [ text label ]


primaryButton : String -> msg -> Html msg
primaryButton label clickMsg =
    button [ class "button primary", onClick clickMsg ] [ text label ]


dangerousButton : String -> msg -> Html msg
dangerousButton label clickMsg =
    button [ class "button dangerous", onClick clickMsg ] [ text label ]


normalButton : String -> msg -> Html msg
normalButton label clickMsg =
    button [ class "button", onClick clickMsg ] [ text label ]


normalSubmitButton : String -> Maybe msg -> Html msg
normalSubmitButton label clickMsg =
    let
        props =
            case clickMsg of
                Just msg ->
                    [ onClick msg ]

                Nothing ->
                    [ disabled True ]
    in
    button (class "button" :: props) [ text label ]
