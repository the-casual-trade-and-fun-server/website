module MaybeExtra exposing (filter)


filter : (a -> Bool) -> Maybe a -> Maybe a
filter shouldFilter maybe =
    maybe
        |> Maybe.andThen
            (\value ->
                if shouldFilter value then
                    Just value

                else
                    Nothing
            )
