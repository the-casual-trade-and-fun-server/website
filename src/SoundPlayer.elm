port module SoundPlayer exposing (pauseSound, playSound, setVolume, soundEnded)


port playSound : String -> Cmd msg


port pauseSound : () -> Cmd msg


port setVolume : Float -> Cmd msg


port soundEnded : (String -> msg) -> Sub msg
