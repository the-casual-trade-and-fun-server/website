module Application exposing (Intention(..), Model, Msg, init, subscriptions, update, updateUrl, view)

import Backend exposing (Authentication(..), Backend, Role(..), User, loginUrl, logoutUrl)
import BrightnessStorage exposing (Brightness(..), brightnessClass)
import Browser
import Html exposing (Html, a, div, footer, i, img, main_, nav, span, text)
import Html.Attributes exposing (class, href, src)
import Html.Events exposing (onClick)
import Page.About as AboutPage
import Page.Home as PHome
import Page.ServerManager.State as SmState
import Page.ServerManager.Types as SmTypes
import Page.ServerManager.View as SmView
import Page.Settings.State as PSettingsState
import Page.Settings.Types as PSettings
import Page.Settings.View as PSettingsView
import Time
import Url
import Utils exposing (withCommand, withoutCommands)
import View.Dialog


type Page
    = Loading
    | Home PHome.Model
    | About
    | Settings PSettings.Model
    | ServerManager SmTypes.Model


type Dialog
    = SuccessDialog String
    | ErrorDialog String String
    | NoDialog


type alias Model =
    { page : Page
    , authentication : Authentication
    , dialog : Dialog
    }


type Intention
    = Switch_to_Light
    | Switch_to_Dark


type Msg
    = Got_Data_for_server_manager Backend.InitialDataForServerManager
    | Failed_to_retrieve_initial_data_for_server_manager String
    | Light_mode_clicked
    | Dark_mode_clicked
    | DialogDismissingClicked
    | HomeMsg PHome.Msg
    | UserSettingsMsg PSettings.Msg
    | ServerManagerMsg SmTypes.Msg


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.page of
        Settings settings ->
            PSettingsState.subscriptions settings |> Sub.map UserSettingsMsg

        Home home ->
            PHome.subscriptions home |> Sub.map HomeMsg

        _ ->
            Sub.none


fulfillsRole : Role -> Role -> Bool
fulfillsRole needed role =
    case needed of
        Player ->
            True

        VIP ->
            role == ServerOwner || role == TeamMember || role == VIP || role == PausedVIP

        PausedVIP ->
            role == PausedVIP

        TeamMember ->
            role == ServerOwner || role == TeamMember

        ServerOwner ->
            role == ServerOwner


homePage : Backend -> ( Page, Cmd Msg )
homePage backend =
    PHome.init backend |> (\( model, cmds ) -> ( Home model, cmds |> Cmd.map HomeMsg ))


pageLoadInitialDataForServerManager : Backend -> ( Page, Cmd Msg )
pageLoadInitialDataForServerManager backend =
    ( Loading
    , Backend.retrieveInitialDataForServerManager
        backend
        Got_Data_for_server_manager
        Failed_to_retrieve_initial_data_for_server_manager
    )


pageOfUrl : Backend -> Url.Url -> Authentication -> ( Page, Cmd Msg )
pageOfUrl backend url authentication =
    case authentication of
        Not_logged_in ->
            case url.path of
                "/about" ->
                    ( About, Cmd.none )

                _ ->
                    homePage backend

        Authentified user ->
            case url.path of
                "/about" ->
                    ( About, Cmd.none )

                "/settings" ->
                    if user.role |> fulfillsRole VIP then
                        PSettingsState.init backend user.steamId |> (\( model, cmds ) -> ( Settings model, cmds |> Cmd.map UserSettingsMsg ))

                    else
                        homePage backend

                "/server-manager" ->
                    if user.role |> fulfillsRole ServerOwner then
                        pageLoadInitialDataForServerManager backend

                    else
                        homePage backend

                _ ->
                    PSettingsState.init backend user.steamId |> (\( model, cmds ) -> ( Settings model, cmds |> Cmd.map UserSettingsMsg ))


updateUrl : Backend -> Url.Url -> Model -> ( Model, Cmd Msg )
updateUrl backend url model =
    let
        ( page, cmd ) =
            pageOfUrl backend url model.authentication
    in
    { model | page = page } |> withCommand cmd


init : Backend -> Url.Url -> Authentication -> ( Model, Cmd Msg )
init backend url authentication =
    let
        ( page, cmd ) =
            pageOfUrl backend url authentication
    in
    { page = page
    , authentication = authentication
    , dialog = NoDialog
    }
        |> withCommand cmd


homeModel : Model -> Maybe PHome.Model
homeModel model =
    case model.page of
        Home home ->
            Just home

        _ ->
            Nothing


updateHome : Backend -> PHome.Msg -> Model -> PHome.Model -> ( Model, Cmd Msg )
updateHome backend msg model home =
    let
        ( newModel, cmds ) =
            PHome.update backend home msg
    in
    ( { model | page = Home newModel }, cmds |> Cmd.map HomeMsg )


withoutIntention : ( model, cmds ) -> ( model, cmds, Maybe Intention )
withoutIntention ( model, cmds ) =
    ( model, cmds, Nothing )


withIntention : Intention -> ( model, cmds ) -> ( model, cmds, Maybe Intention )
withIntention intention ( model, cmds ) =
    ( model, cmds, Just intention )


settingsModel : Model -> Maybe ( User, PSettings.Model )
settingsModel model =
    case ( model.authentication, model.page ) of
        ( Authentified user, Settings settings ) ->
            Just ( user, settings )

        _ ->
            Nothing


resolveSettingsIntention : Maybe PSettings.Intention -> Model -> Model
resolveSettingsIntention intention model =
    case intention of
        Just (PSettings.NotifySuccess info) ->
            { model | dialog = SuccessDialog info }

        Just (PSettings.NotifyError error details) ->
            { model | dialog = ErrorDialog error details }

        Nothing ->
            model


updateSettings : Backend.Backend -> Model -> PSettings.Msg -> ( User, PSettings.Model ) -> ( Model, Cmd Msg )
updateSettings backend model settingsMsg ( user, subModel ) =
    let
        ( newModel, cmd, intention ) =
            PSettingsState.update backend user.token user.steamId settingsMsg subModel
    in
    { model | page = Settings newModel }
        |> resolveSettingsIntention intention
        |> withCommand (cmd |> Cmd.map UserSettingsMsg)


serverManagerModel : Model -> Maybe ( User, SmTypes.Model )
serverManagerModel model =
    case ( model.authentication, model.page ) of
        ( Authentified user, ServerManager smModel ) ->
            Just ( user, smModel )

        _ ->
            Nothing


resolveServerManagerIntention : SmTypes.Intention -> Model -> Model
resolveServerManagerIntention intention model =
    case intention of
        SmTypes.NotifySuccess info ->
            { model | dialog = SuccessDialog info }

        SmTypes.NotifyError error details ->
            { model | dialog = ErrorDialog error details }

        SmTypes.No_Intention ->
            model


updateServerManager : Backend -> Model -> SmTypes.Msg -> ( User, SmTypes.Model ) -> ( Model, Cmd Msg )
updateServerManager backend model smMsg ( user, smModel ) =
    let
        ( newModel, cmds, intention ) =
            SmState.update backend user.token smModel smMsg
    in
    { model | page = ServerManager newModel }
        |> resolveServerManagerIntention intention
        |> withCommand (cmds |> Cmd.map ServerManagerMsg)


update : Backend -> Msg -> Model -> ( Model, Cmd Msg, Maybe Intention )
update backend msg model =
    case msg of
        Got_Data_for_server_manager data ->
            { model | page = ServerManager (SmState.init data.vips data.members) }
                |> withoutCommands
                |> withoutIntention

        Failed_to_retrieve_initial_data_for_server_manager error ->
            { model | dialog = ErrorDialog "Can't retrieve Data for Server-Manager" error }
                |> withoutCommands
                |> withoutIntention

        Light_mode_clicked ->
            model
                |> withoutCommands
                |> withIntention Switch_to_Light

        Dark_mode_clicked ->
            model
                |> withoutCommands
                |> withIntention Switch_to_Dark

        HomeMsg homeMsg ->
            homeModel model
                |> Maybe.map (updateHome backend homeMsg model)
                |> Maybe.withDefault ( model, Cmd.none )
                |> withoutIntention

        UserSettingsMsg settingsMsg ->
            settingsModel model
                |> Maybe.map (updateSettings backend model settingsMsg)
                |> Maybe.withDefault ( model, Cmd.none )
                |> withoutIntention

        ServerManagerMsg smMsg ->
            serverManagerModel model
                |> Maybe.map (updateServerManager backend model smMsg)
                |> Maybe.withDefault ( model, Cmd.none )
                |> withoutIntention

        DialogDismissingClicked ->
            { model | dialog = NoDialog }
                |> withoutCommands
                |> withoutIntention


viewNavigationItem : String -> ( String, String ) -> Html Msg
viewNavigationItem activeRoute ( label, route ) =
    if activeRoute == route then
        span [ class "nav-item active" ] [ text label ]

    else
        a [ class "nav-item", href route ] [ text label ]


viewNavigation : String -> Role -> Html Msg
viewNavigation activeRoute role =
    [ ( "Home", "/home", Player )
    , ( "Settings", "/settings", VIP )
    , ( "Manage Server", "/server-manager", ServerOwner )
    ]
        |> List.filter (\( _, _, neededRole ) -> role |> fulfillsRole neededRole)
        |> List.map (\( a, b, _ ) -> ( a, b ))
        |> List.map (viewNavigationItem activeRoute)
        |> nav [ class "flex-grow-0 flex flex-row full-width" ]


viewLogin : Backend -> Html Msg
viewLogin backend =
    a [ backend |> loginUrl |> href ]
        [ img [ class "mx-4", src "/assets/steam_login.png" ] [] ]


viewLogout : Backend -> Html Msg
viewLogout backend =
    a [ backend |> logoutUrl |> href ]
        [ i [ class "fas fa-sign-out-alt hover:text-lighter" ] [] ]


viewUser : Backend -> User -> Html Msg
viewUser backend user =
    div [ class "flex items-center px-4" ]
        [ img [ src user.avatar, class "rounded-full" ] []
        , span [ class "mx-2 text-lg" ] [ text user.name ]
        , viewLogout backend
        ]


viewAuthentication : Backend -> Authentication -> Html Msg
viewAuthentication backend auth =
    case auth of
        Authentified user ->
            viewUser backend user

        _ ->
            viewLogin backend


viewBrightnessToggle : Brightness -> Html Msg
viewBrightnessToggle current =
    case current of
        Light ->
            i [ class "fas fa-moon p-2 text-lg hover:text-lighter cursor-pointer", onClick Dark_mode_clicked ] []

        Dark ->
            i [ class "fas fa-sun p-2 text-lg hover:text-lighter cursor-pointer", onClick Light_mode_clicked ] []


roleOfAuthentication : Authentication -> Role
roleOfAuthentication auth =
    case auth of
        Authentified user ->
            user.role

        Not_logged_in ->
            Player


viewHeader : String -> Backend -> Brightness -> Authentication -> Html Msg
viewHeader activeRoute backend brightness auth =
    div [ class "sticky z-10 top-0 flex row bg-frame shadow-md" ]
        [ div [ class "self-center mx-3" ] [ viewBrightnessToggle brightness ]
        , div [ class "flex-grow" ] [ viewNavigation activeRoute (roleOfAuthentication auth) ]
        , div [ class "self-center" ] [ viewAuthentication backend auth ]
        ]


viewFooter : Html Msg
viewFooter =
    footer [ class "py-2 shadow bg-frame text-center" ] [ text "Copyright © 2019 by Shadow_Man" ]


viewDialog : Dialog -> Html Msg
viewDialog dialog =
    case dialog of
        SuccessDialog info ->
            View.Dialog.success info DialogDismissingClicked

        ErrorDialog info err ->
            View.Dialog.error info err DialogDismissingClicked

        NoDialog ->
            div [] []


pageAsUrl : Page -> String
pageAsUrl page =
    case page of
        Loading ->
            "/"

        Home _ ->
            "/home"

        About ->
            "/about"

        Settings _ ->
            "/settings"

        ServerManager _ ->
            "/serverManager"


viewContainer : Brightness -> Backend -> Model -> List (Html Msg) -> Html Msg
viewContainer brightness backend model content =
    main_ [ class <| "min-h-screen h-full flex flex-col " ++ brightnessClass brightness ]
        [ viewHeader (pageAsUrl model.page) backend brightness model.authentication
        , div [ class "flex-grow container self-center" ] content
        , viewFooter
        , viewDialog model.dialog
        ]


viewBody : Time.Zone -> Brightness -> Backend -> Model -> Html Msg
viewBody timezone brightness backend model =
    case model.page of
        Loading ->
            viewContainer brightness backend model [ i [ class "text-4xl fas fa-spinner fa-pulse" ] [] ]

        Home home ->
            PHome.view backend home |> Html.map HomeMsg

        About ->
            AboutPage.view

        Settings settings ->
            viewContainer brightness backend model [ PSettingsView.view settings |> Html.map UserSettingsMsg ]

        ServerManager serverManager ->
            viewContainer brightness backend model [ SmView.view timezone serverManager |> Html.map ServerManagerMsg ]


title : Page -> String
title page =
    case page of
        Loading ->
            "The Casual Trade and Fun Server"

        Home _ ->
            "Welcome"

        About ->
            "About"

        Settings _ ->
            "Settings"

        ServerManager _ ->
            "Server Manager"


view : Time.Zone -> Brightness -> Backend -> Model -> Browser.Document Msg
view timezone brightness backend model =
    { title = title model.page
    , body = [ viewBody timezone brightness backend model ]
    }
