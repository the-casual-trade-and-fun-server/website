module Backend exposing
    ( Authentication(..)
    , Backend
    , CorrectVipTimeError(..)
    , CustomChatColors
    , DiscordInfo
    , ExtendTimeForAllVipsError(..)
    , ExtendVipTimeError(..)
    , GiveUserVipError(..)
    , InitialDataForServerManager
    , InitialDataForSettings
    , Joinsound
    , Member
    , PlayerOnServer
    , Rank(..)
    , Role(..)
    , ServerInfo
    , ServerManagerError(..)
    , ServerStatus(..)
    , SteamGroup
    , Token
    , TransferTimeError(..)
    , User
    , Vip
    , correctTime
    , declareUserAsMember
    , extendTime
    , extendTimeForAllVips
    , fromUrl
    , giveUserVIP
    , loginUrl
    , logoutUrl
    , onlineUsersOnDiscord
    , parseRank
    , pauseVip
    , removeDueToRuleBreaking
    , removeMember
    , resumeVip
    , retrieveAllMembers
    , retrieveAllVips
    , retrieveInitialDataForServerManager
    , retrieveInitialDataForSettings
    , retrieveUser
    , serverStatus
    , setCustomChatColors
    , setJoinsound
    , setRankOfMember
    , steamGroup
    , transferTime
    , useNoJoinsound
    )

import Http
import Json.Decode exposing (Decoder, andThen, bool, fail, field, float, int, list, map, map2, map3, map4, map5, map8, nullable, string, succeed)
import Json.Encode as Encode
import Platform.Cmd exposing (Cmd)
import Time


type Backend
    = Backend String


type Token
    = Token Int


type Role
    = ServerOwner
    | TeamMember
    | VIP
    | PausedVIP
    | Player


type alias User =
    { steamId : String
    , role : Role
    , name : String
    , avatar : String
    , token : Token
    }


type Authentication
    = Not_logged_in
    | Authentified User


type alias PlayerOnServer =
    { name : String
    , score : Int
    , duration : Float
    }


type alias ServerInfo =
    { players : List PlayerOnServer
    , address : String
    , maxPlayers : Int
    , map : String
    , image : String
    }


type ServerStatus
    = Server_is_online ServerInfo
    | Server_is_offline String


type alias DiscordInfo =
    { online : Int
    , invite : String
    }


type alias SteamGroup =
    { id : String
    , members : Maybe Int
    }


type alias Vip =
    { id : String
    , name : String
    , avatar : String
    , date : Time.Posix
    , duration : Int
    , duraitonFromPauses : Int
    , countOfPauses : Int
    , pausedSince : Maybe Time.Posix
    }


type Rank
    = Owner
    | Admin
    | Moderator
    | Mapper
    | PermanentVip


type alias Member =
    { id : String
    , name : String
    , avatar : String
    , rank : Rank
    }


type alias InitialDataForServerManager =
    { members : List Member
    , vips : List Vip
    }


type GiveUserVipError
    = AlreadyVip
    | CurrentlyPaused
    | IsRuleBreaker
    | IsMember
    | UnknownGiveUserVipError String


type TransferTimeError
    = DurationIsNotAvailable
    | VIPIsExpired
    | UnknownTransferTimeError String


type ExtendVipTimeError
    = Extend_VipIsExpired
    | UnknownExtendVipTimeError String


type ExtendTimeForAllVipsError
    = DurationOfZero
    | UnknownExtendTimeForAllVipsError String


type CorrectVipTimeError
    = Correct_VipIsExpired
    | UnknownCorrectVipTimeError String


type ServerManagerError err
    = Offline
    | ServerOffline
    | Failed err
    | InternalError String


type alias Joinsound =
    { name : String
    , previewUrl : String
    , used : Bool
    }


type alias CustomChatColors =
    { tag : Maybe String
    , name : Maybe String
    , chat : Maybe String
    }


type alias InitialDataForSettings =
    { joinsound : Maybe String
    , definedJoinsounds : List Joinsound
    , customChatColors : CustomChatColors
    }


type alias Command err msg =
    Backend -> Token -> msg -> (ServerManagerError err -> msg) -> Cmd msg


fromUrl : String -> Backend
fromUrl =
    Backend


loginUrl : Backend -> String
loginUrl (Backend url) =
    url ++ "/login.php"


logoutUrl : Backend -> String
logoutUrl (Backend url) =
    url ++ "/logout.php"


decodeRole : Decoder Role
decodeRole =
    string
        |> map
            (\role ->
                case role of
                    "server-owner" ->
                        ServerOwner

                    "team-member" ->
                        TeamMember

                    "vip" ->
                        VIP

                    "paused-vip" ->
                        PausedVIP

                    _ ->
                        Player
            )


decodeToken : Decoder Token
decodeToken =
    map Token int


decodeUser : Decoder User
decodeUser =
    map5
        User
        (field "steamId" string)
        (field "role" decodeRole)
        (field "name" string)
        (field "avatar" string)
        (field "token" decodeToken)


decodeAuthentication : Decoder (Result String Authentication)
decodeAuthentication =
    field "type" string
        |> andThen
            (\typeinfo ->
                case typeinfo of
                    "user" ->
                        map (Authentified >> Ok) decodeUser

                    "not-logged-in" ->
                        succeed (Ok Not_logged_in)

                    "error" ->
                        field "msg" string |> map Err

                    unknow ->
                        fail <| "Unknown authentication " ++ unknow
            )


asErrorReasonOrDefault : String -> Http.Error -> String
asErrorReasonOrDefault default error =
    case error of
        Http.Timeout ->
            "It took to long to recieve your authentication state. Please try again"

        Http.NetworkError ->
            "You are not online. Please connect to the internet and try again"

        _ ->
            default


httpResultAsAuthentication : Result Http.Error (Result String Authentication) -> Result String Authentication
httpResultAsAuthentication result =
    case result of
        Ok auth ->
            auth

        Err err ->
            err
                |> asErrorReasonOrDefault "It seems that I can't authenticate you. Please inform a team member"
                |> Err


resultAsMsg : (res -> msg) -> (err -> msg) -> Result err res -> msg
resultAsMsg successMsg errorMsg result =
    case result of
        Ok data ->
            successMsg data

        Err err ->
            errorMsg err


retrieveUser : Backend -> (Result String Authentication -> msg) -> Cmd msg
retrieveUser (Backend backend) msg =
    Http.get
        { url = backend ++ "/authentication.php"
        , expect = Http.expectJson (httpResultAsAuthentication >> msg) decodeAuthentication
        }


decodePlayerOnServer : Decoder PlayerOnServer
decodePlayerOnServer =
    map3
        PlayerOnServer
        (field "name" string)
        (field "score" int)
        (field "duration" float)


decodeServerInfo : Decoder ServerInfo
decodeServerInfo =
    map5
        ServerInfo
        (field "players" (list decodePlayerOnServer))
        (field "address" string)
        (field "maxPlayers" int)
        (field "map" string)
        (field "image" string)


decodeServerOfflineReason : Decoder ServerStatus
decodeServerOfflineReason =
    field "reason" string
        |> map Server_is_offline


decodeServerStatus : Decoder ServerStatus
decodeServerStatus =
    field "online" bool
        |> andThen
            (\isOnline ->
                if isOnline then
                    decodeServerInfo |> map Server_is_online

                else
                    decodeServerOfflineReason
            )


httpResultAsServerStatus : Result Http.Error ServerStatus -> Result String ServerStatus
httpResultAsServerStatus res =
    res
        |> Result.mapError (asErrorReasonOrDefault "It seems that I can't load the Server status. Please inform a team member")


serverStatus : Backend -> (ServerStatus -> msg) -> (String -> msg) -> Cmd msg
serverStatus (Backend backend) successMsg errorMsg =
    Http.get
        { url = backend ++ "/getServerStatus.php"
        , expect = Http.expectJson (httpResultAsServerStatus >> resultAsMsg successMsg errorMsg) decodeServerStatus
        }


mapHttpErrorToStringWithDefault : String -> Result Http.Error data -> Result String data
mapHttpErrorToStringWithDefault default res =
    res |> Result.mapError (asErrorReasonOrDefault default)


decodeSteamGroup : Decoder SteamGroup
decodeSteamGroup =
    map2
        SteamGroup
        (field "id" string)
        (field "members" (nullable int))


steamGroup : Backend -> (SteamGroup -> msg) -> (String -> msg) -> Cmd msg
steamGroup (Backend backend) successMsg errorMsg =
    Http.get
        { url = backend ++ "/getSteamGroup.php"
        , expect = Http.expectJson (mapHttpErrorToStringWithDefault "Can't load members in Steam group" >> resultAsMsg successMsg errorMsg) decodeSteamGroup
        }


decodeDiscordInfo : Decoder DiscordInfo
decodeDiscordInfo =
    map2
        DiscordInfo
        (field "online" int)
        (field "invite" string)


onlineUsersOnDiscord : Backend -> (DiscordInfo -> msg) -> (String -> msg) -> Cmd msg
onlineUsersOnDiscord (Backend backend) successMsg errorMsg =
    Http.get
        { url = backend ++ "/getOnlineUsersOnDiscord.php"
        , expect = Http.expectJson (mapHttpErrorToStringWithDefault "Can't load online users on Discord" >> resultAsMsg successMsg errorMsg) decodeDiscordInfo
        }


httpResultAsAllVips : Result Http.Error (List Vip) -> Result String (List Vip)
httpResultAsAllVips res =
    res
        |> Result.mapError (asErrorReasonOrDefault "It seems that I can't load VIPs. Please inform a team member")


decodeVip : Decoder Vip
decodeVip =
    map8
        Vip
        (field "id" string)
        (field "name" string)
        (field "avatar" string)
        (field "date" int |> map Time.millisToPosix)
        (field "duration" int)
        (field "durationFromPauses" int)
        (field "countOfPauses" int)
        (field "pausedSince" (nullable (int |> map Time.millisToPosix)))


decodeVips : Decoder (List Vip)
decodeVips =
    list decodeVip


httpResultAsAllMembers : Result Http.Error (List Member) -> Result String (List Member)
httpResultAsAllMembers res =
    res |> Result.mapError (asErrorReasonOrDefault "It seems that I can't load Members. Please inform a team member")


httpResultAsInitialDataForServerManager : Result Http.Error InitialDataForServerManager -> Result String InitialDataForServerManager
httpResultAsInitialDataForServerManager res =
    res |> Result.mapError (asErrorReasonOrDefault "It seems that I can't load the data for this page. Please inform a team member")


simplifyHttpError : Result Http.Error data -> Result String data
simplifyHttpError res =
    res |> Result.mapError (asErrorReasonOrDefault "Something went horrible wrong. Please inform a team member")


parseRank : String -> Maybe Rank
parseRank rank =
    case rank of
        "owner" ->
            Just Owner

        "admin" ->
            Just Admin

        "moderator" ->
            Just Moderator

        "mapper" ->
            Just Mapper

        "permanent-vip" ->
            Just PermanentVip

        _ ->
            Nothing


decodeRank : Decoder Rank
decodeRank =
    string
        |> andThen (parseRank >> Maybe.map Json.Decode.succeed >> Maybe.withDefault (Json.Decode.fail "Rank can't be parsed"))


encodeRank : Rank -> Encode.Value
encodeRank rank =
    (case rank of
        Owner ->
            "owner"

        Admin ->
            "admin"

        Moderator ->
            "moderator"

        Mapper ->
            "mapper"

        PermanentVip ->
            "permanent-vip"
    )
        |> Encode.string


decodeMember : Decoder Member
decodeMember =
    map4
        Member
        (field "id" string)
        (field "name" string)
        (field "avatar" string)
        (field "rank" decodeRank)


decodeMembers : Decoder (List Member)
decodeMembers =
    list decodeMember


decodeInitialDataForServerManager : Decoder InitialDataForServerManager
decodeInitialDataForServerManager =
    map2
        InitialDataForServerManager
        (field "members" decodeMembers)
        (field "vips" decodeVips)


retrieveAllVips : Backend -> (List Vip -> msg) -> (String -> msg) -> Cmd msg
retrieveAllVips (Backend backend) successMsg errorMsg =
    Http.get
        { url = backend ++ "/getAllVips.php"
        , expect = Http.expectJson (httpResultAsAllVips >> resultAsMsg successMsg errorMsg) decodeVips
        }


retrieveAllMembers : Backend -> (List Member -> msg) -> (String -> msg) -> Cmd msg
retrieveAllMembers (Backend backend) successMsg errorMsg =
    Http.get
        { url = backend ++ "/getAllMembers.php"
        , expect = Http.expectJson (httpResultAsAllMembers >> resultAsMsg successMsg errorMsg) decodeMembers
        }


retrieveInitialDataForServerManager : Backend -> (InitialDataForServerManager -> msg) -> (String -> msg) -> Cmd msg
retrieveInitialDataForServerManager (Backend backend) successMsg errorMsg =
    Http.get
        { url = backend ++ "/getInitialDataForServerManager.php"
        , expect = Http.expectJson (httpResultAsInitialDataForServerManager >> resultAsMsg successMsg errorMsg) decodeInitialDataForServerManager
        }


parseGiveUserVipError : String -> GiveUserVipError
parseGiveUserVipError input =
    case input of
        "UserIsAlreadyVIP" ->
            AlreadyVip

        "UserIsCurrentlyPaused" ->
            CurrentlyPaused

        "UserIsARuleBreaker" ->
            IsRuleBreaker

        "UserIsAMember" ->
            IsMember

        err ->
            UnknownGiveUserVipError err


httpErrorAsError : Http.Error -> ServerManagerError err
httpErrorAsError err =
    case err of
        Http.Timeout ->
            Offline

        Http.NetworkError ->
            Offline

        Http.BadStatus status ->
            ("Status " ++ String.fromInt status) |> InternalError

        Http.BadBody msg ->
            InternalError msg

        Http.BadUrl _ ->
            InternalError "Url leads to void"


parseResult : (String -> err) -> msg -> (ServerManagerError err -> msg) -> Result Http.Error String -> msg
parseResult mapError successMsg errorMsg result =
    case result of
        Ok response ->
            if String.isEmpty response then
                successMsg

            else if response == "offline" then
                errorMsg ServerOffline

            else
                response |> mapError |> Failed |> errorMsg

        Err err ->
            errorMsg (httpErrorAsError err)


encodeCommand : Token -> String -> Encode.Value -> Encode.Value
encodeCommand (Token token) command args =
    Encode.object
        [ ( "token", Encode.int token )
        , ( "command", Encode.string command )
        , ( "args", args )
        ]


executeCommand : String -> Encode.Value -> (String -> error) -> Backend -> Token -> msg -> (ServerManagerError error -> msg) -> Cmd msg
executeCommand command args errorParser (Backend backend) token successMsg errorMsg =
    Http.post
        { url = backend ++ "/command.php"
        , body = Http.jsonBody (encodeCommand token command args)
        , expect = Http.expectString (parseResult errorParser successMsg errorMsg)
        }


encodeGiveUserVipCommand : String -> Int -> Encode.Value
encodeGiveUserVipCommand user duration =
    Encode.object
        [ ( "user", Encode.string user )
        , ( "duration", Encode.int duration )
        ]


giveUserVIP : String -> Int -> Command GiveUserVipError msg
giveUserVIP user duration =
    executeCommand
        "giveUserVip"
        (encodeGiveUserVipCommand user duration)
        parseGiveUserVipError


encodeTransferTimeCommand : String -> String -> Int -> Encode.Value
encodeTransferTimeCommand vip receiver duration =
    Encode.object
        [ ( "user", Encode.string vip )
        , ( "target", Encode.string receiver )
        , ( "duration", Encode.int duration )
        ]


parseTransferTimeError : String -> TransferTimeError
parseTransferTimeError err =
    case err of
        "DurationIsNotAvailable" ->
            DurationIsNotAvailable

        "VIPIsExpired" ->
            VIPIsExpired

        unkown ->
            UnknownTransferTimeError unkown


transferTime : String -> String -> Int -> Command TransferTimeError msg
transferTime vip receiver duration =
    executeCommand
        "transferTime"
        (encodeTransferTimeCommand vip receiver duration)
        parseTransferTimeError


encodeExtendTimeCommand : String -> Int -> Encode.Value
encodeExtendTimeCommand vip duration =
    Encode.object
        [ ( "user", Encode.string vip )
        , ( "duration", Encode.int duration )
        ]


parseExtendVipTimeError : String -> ExtendVipTimeError
parseExtendVipTimeError input =
    case input of
        "VIPIsExpired" ->
            Extend_VipIsExpired

        err ->
            UnknownExtendVipTimeError err


extendTime : String -> Int -> Command ExtendVipTimeError msg
extendTime vip duration =
    executeCommand
        "extendTime"
        (encodeExtendTimeCommand vip duration)
        parseExtendVipTimeError


encodeExtendTimeForAllVipsCommand : Int -> Encode.Value
encodeExtendTimeForAllVipsCommand duration =
    Encode.object [ ( "duration", Encode.int duration ) ]


parseExtendTimeForAllVipsError : String -> ExtendTimeForAllVipsError
parseExtendTimeForAllVipsError input =
    case input of
        "durationOfZero" ->
            DurationOfZero

        err ->
            UnknownExtendTimeForAllVipsError err


extendTimeForAllVips : Int -> Command ExtendTimeForAllVipsError msg
extendTimeForAllVips duration =
    executeCommand
        "extendTimeForAllVips"
        (encodeExtendTimeForAllVipsCommand duration)
        parseExtendTimeForAllVipsError


encodeCorrectTimeCommand : String -> Int -> Encode.Value
encodeCorrectTimeCommand vip duration =
    Encode.object
        [ ( "user", Encode.string vip )
        , ( "duration", Encode.int duration )
        ]


parseCorrectVipTimeError : String -> CorrectVipTimeError
parseCorrectVipTimeError input =
    case input of
        "VIPIsExpired" ->
            Correct_VipIsExpired

        err ->
            UnknownCorrectVipTimeError err


correctTime : String -> Int -> Command CorrectVipTimeError msg
correctTime vip duration =
    executeCommand
        "correctTime"
        (encodeCorrectTimeCommand vip duration)
        parseCorrectVipTimeError


encodePauseVipCommand : String -> Encode.Value
encodePauseVipCommand vip =
    Encode.object [ ( "user", Encode.string vip ) ]


usePlainError : String -> String
usePlainError error =
    error


pauseVip : String -> Command String msg
pauseVip vip =
    executeCommand
        "pauseVip"
        (encodePauseVipCommand vip)
        usePlainError


encodeResumeVipCommand : String -> Encode.Value
encodeResumeVipCommand vip =
    Encode.object [ ( "user", Encode.string vip ) ]


resumeVip : String -> Command String msg
resumeVip vip =
    executeCommand
        "resumeVip"
        (encodeResumeVipCommand vip)
        usePlainError


encodeRemoveDueToRuleBreakingCommand : String -> Encode.Value
encodeRemoveDueToRuleBreakingCommand vip =
    Encode.object [ ( "vip", Encode.string vip ) ]


removeDueToRuleBreaking : String -> Command String msg
removeDueToRuleBreaking vip =
    executeCommand
        "removeDueToRuleBreaking"
        (encodeRemoveDueToRuleBreakingCommand vip)
        usePlainError


encodeDeclareUserAsMemberCommand : String -> Rank -> Encode.Value
encodeDeclareUserAsMemberCommand user rank =
    Encode.object
        [ ( "user", Encode.string user )
        , ( "rank", encodeRank rank )
        ]


declareUserAsMember : String -> Rank -> Command String msg
declareUserAsMember user rank =
    executeCommand
        "declareUserAsMember"
        (encodeDeclareUserAsMemberCommand user rank)
        usePlainError


encodeSetRankOfMemberCommand : String -> Rank -> Encode.Value
encodeSetRankOfMemberCommand member rank =
    Encode.object
        [ ( "member", Encode.string member )
        , ( "rank", encodeRank rank )
        ]


setRankOfMember : String -> Rank -> Command String msg
setRankOfMember member rank =
    executeCommand
        "setRankOfMember"
        (encodeSetRankOfMemberCommand member rank)
        usePlainError


encodeRemoveMemberCommand : String -> Encode.Value
encodeRemoveMemberCommand member =
    Encode.object [ ( "member", Encode.string member ) ]


removeMember : String -> Command String msg
removeMember member =
    executeCommand
        "removeMember"
        (encodeRemoveMemberCommand member)
        usePlainError


decodeJoinsound : Decoder Joinsound
decodeJoinsound =
    map3
        Joinsound
        (field "Name" string)
        (field "File" string)
        (field "Used" bool)


encodeCustomChatColors : Decoder CustomChatColors
encodeCustomChatColors =
    map3
        CustomChatColors
        (field "tag" (nullable string))
        (field "name" (nullable string))
        (field "chat" (nullable string))


decodeInitialDataForSettings : Decoder InitialDataForSettings
decodeInitialDataForSettings =
    map3
        InitialDataForSettings
        (field "joinsound" (nullable string))
        (field "definedJoinsounds" (list decodeJoinsound))
        (field "customChatColors" encodeCustomChatColors)


retrieveInitialDataForSettings : Backend -> String -> (InitialDataForSettings -> msg) -> (String -> msg) -> Cmd msg
retrieveInitialDataForSettings (Backend backend) steamid successMsg errorMsg =
    Http.get
        { url = backend ++ "/getInitialDataForSettings.php?user=" ++ steamid
        , expect = Http.expectJson (simplifyHttpError >> resultAsMsg successMsg errorMsg) decodeInitialDataForSettings
        }


encodeSetJoinsoundCommand : String -> String -> Encode.Value
encodeSetJoinsoundCommand user sound =
    Encode.object
        [ ( "user", Encode.string user )
        , ( "sound", Encode.string sound )
        ]


setJoinsound : String -> String -> Command String msg
setJoinsound user joinsound =
    executeCommand
        "setJoinsound"
        (encodeSetJoinsoundCommand user joinsound)
        usePlainError


encodeUseNoJoinsoundCommand : String -> Encode.Value
encodeUseNoJoinsoundCommand user =
    Encode.object [ ( "user", Encode.string user ) ]


useNoJoinsound : String -> Command String msg
useNoJoinsound user =
    executeCommand
        "useNoJoinsound"
        (encodeUseNoJoinsoundCommand user)
        usePlainError


encodeColors : CustomChatColors -> Encode.Value
encodeColors colors =
    Encode.object
        [ ( "tag", colors.tag |> Maybe.map Encode.string |> Maybe.withDefault Encode.null )
        , ( "name", colors.name |> Maybe.map Encode.string |> Maybe.withDefault Encode.null )
        , ( "chat", colors.chat |> Maybe.map Encode.string |> Maybe.withDefault Encode.null )
        ]


encodeSetCustomChatColorsCommand : String -> CustomChatColors -> Encode.Value
encodeSetCustomChatColorsCommand user colors =
    Encode.object
        [ ( "user", Encode.string user )
        , ( "colors", encodeColors colors )
        ]


setCustomChatColors : String -> CustomChatColors -> Command String msg
setCustomChatColors user colors =
    executeCommand
        "setCustomChatColors"
        (encodeSetCustomChatColorsCommand user colors)
        usePlainError
