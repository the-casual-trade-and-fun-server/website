<?php
    require __DIR__ . "/vars.php";
    header('Content-Type: application/json');

    if (!isset($STEAM_GROUP_ID)) {
        echo json_encode(array(
            "id" => "casual_trade_fun",
            "members" => random_int(0, 10000)
        ));
        exit;
    }

    $groupInfo = file_get_contents("http://steamcommunity.com/groups/$STEAM_GROUP_ID/memberslistxml/?xml=1");
    echo json_encode(array(
        "id" => $STEAM_GROUP_ID,
        "members" => $groupInfo != null ? (int)simplexml_load_string($groupInfo)->groupDetails->memberCount : null
    ));
?>