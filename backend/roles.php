<?php
    $ROLE_SERVER_OWNER = "server-owner";
    $ROLE_TEAM_MEMBER = "team-member";
    $ROLE_VIP = "vip";

    function EnsureRightsTo($requiredRole, $token)
    {
        session_start();
        $auth = $_SESSION["auth"];

        $hasAccess =
            $auth != null
            && $auth["token"] == $token
            && fulfillsRole($auth["role"], $requiredRole);

        if (!$hasAccess) {
            echo "Missing right to do this action";
            exit;
        }
    }

    function fulfillsRole($role, $requires)
    {
        return weightOfRole($role) >= weightOfRole($requires);
    }

    function weightOfRole($role)
    {
        global $ROLE_SERVER_OWNER, $ROLE_TEAM_MEMBER, $ROLE_VIP;
        if ($role == $ROLE_SERVER_OWNER) return 1000;
        if ($role == $ROLE_TEAM_MEMBER) return 100;
        if ($role == $ROLE_VIP) return 10;
        return 0;
    }
?>