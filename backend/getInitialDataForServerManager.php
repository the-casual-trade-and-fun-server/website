<?php
    require __DIR__ . "/vars.php";
    require __DIR__ . "/helpers.php";
    require __DIR__ . "/steamWebApi.php";
    header('Content-Type: application/json');

    $loadedMembers = file_get_contents("$SERVER_MANAGER_URL/getMembers");
    $loadedVips = file_get_contents("$SERVER_MANAGER_URL/getVIPs");
    if ($loadedMembers == null || $loadedVips == null) {
        echo "offline";
        exit;
    }

    $members =
        asHashSet(
            function($member) { return SteamIdAsSteam64($member->id); },
            json_decode($loadedMembers)
        );

    $vips =
        asHashSet(
            function($vip) { return SteamIdAsSteam64($vip->id); },
            json_decode($loadedVips)
        );

    $membersToFetch = array_merge(array_keys($members), array_keys($vips));
    $summaries =
        asHashSet(
            function($summary) { return $summary->steamid; },
            isset($DEMO_USER) ? GetDemoSummaries($membersToFetch) : GetPlayerSummaries($STEAM_API_KEY, $membersToFetch)
        );

    $allMembers = array();
    foreach ($members as $key => $info)
    {
        $summary = $summaries[$key];
        array_push($allMembers, array(
            "id" => $info->id,
            "name" => $summary->personaname,
            "avatar" => $summary->avatarmedium,
            "rank" => $info->rank
        ));
    }

    $allVips = array();
    foreach($vips as $key => $info)
    {
        $summary = $summaries[$key];
        array_push($allVips, array(
            "id" => $info->id,
            "name" => $summary->personaname,
            "avatar" => $summary->avatarmedium,
            "date" => $info->date,
            "duration" => $info->duration,
            "durationFromPauses" => $info->durationFromPauses,
            "countOfPauses" => $info->countOfPauses,
            "pausedSince" => optionToNullable($info->pausedSince)
        ));
    }

    echo json_encode(array(
        "members" => $allMembers,
        "vips" => $allVips
    ));
?>