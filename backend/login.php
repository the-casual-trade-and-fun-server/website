<?php
    require __DIR__ . '/../vendor/autoload.php';
    require __DIR__ . '/vars.php';
    require __DIR__ . '/steamWebApi.php';

    $OpenIdProvider = "https://steamcommunity.com/openid";

    function GetRoleOf($serverManagerUrl, $communityId)
    {
        $steamId = Steam64AsSteamId($communityId);
        $apiCall = "$serverManagerUrl/getRoleOf?steamid=$steamId";
        return json_decode(file_get_contents($apiCall));
    }

    function RoleOfDemoUser($user) {
        switch ($user) {
            case 1:
                return "vip";

            case 2:
                return "paused-vip";

            case 3:
                return "team-member";

            case 4:
                return "server-owner";

            default:
                return "player";
        }
    }

    try {
        if (isset($DEMO_USER)) {
            session_start();
            $role = RoleOfDemoUser($DEMO_USER);
            $_SESSION["auth"] = array(
                "type" => "user",
                "steamId" => "76561197960267726",
                "role" => $role,
                "name" => "anonymous ($role)",
                "avatar" => "",
                "token" => rand()
            );
            header("Location: /");
        }
        else {
            $openid = new LightOpenID($_SERVER["HTTP_HOST"]);
            if(!$openid->mode) {
                $openid->identity = $OpenIdProvider;
                header("Location: ". $openid->authUrl());
            } elseif($openid->mode == 'cancel') {
                header("Location: /");
            } else {
                session_start();
                $openid->validate();
                $id = str_replace("$OpenIdProvider/id/", "",$openid->identity);

                $userinfo = GetPlayerSummaries($STEAM_API_KEY, array($id))[0];
                $role = GetRoleOf($SERVER_MANAGER_URL, $id);

                $_SESSION["auth"] = array(
                    "type" => "user",
                    "steamId" => $id,
                    "role" => $role->success ? $role->role : "player",
                    "name" => $userinfo->personaname,
                    "avatar" => $userinfo->avatar,
                    "token" => rand()
                );
                header("Location: /");
            }
        }
    } catch(ErrorException $e) {
        session_start();
        $_SESSION["auth"] = array("type" => "error", "msg" => $e->getMessage());
        header("Location: /");
    }
?>