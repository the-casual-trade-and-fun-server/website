<?php
    function asHashSet($map, $array)
    {
        $new = array();
        foreach ($array as $value) {
            $new[$map($value)] = $value;
        }
        return $new;
    }

    function optionToNullable($option) {
        return empty($option) ? null : $option[0];
    }
?>