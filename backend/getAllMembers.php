<?php
    require __DIR__ . "/vars.php";
    require __DIR__ . "/helpers.php";
    require __DIR__ . "/steamWebApi.php";
    header('Content-Type: application/json');

    $loadedMembers = file_get_contents("$SERVER_MANAGER_URL/getMembers");
    if ($loadedMembers == null) {
        echo "offline";
        exit;
    }

    $members =
        asHashSet(
            function($member) { return SteamIdAsSteam64($member->id); },
            json_decode($loadedMembers)
        );

    $membersToFetch = array_keys($members);
    $summaries =
        asHashSet(
            function($summary) { return $summary->steamid; },
            isset($DEMO_USER) ? GetDemoSummaries($membersToFetch) : GetPlayerSummaries($STEAM_API_KEY, $membersToFetch)
        );

    $allMembers = array();
    foreach ($members as $key => $info)
    {
        $summary = $summaries[$key];
        array_push($allMembers, array(
            "id" => $info->id,
            "name" => $summary->personaname,
            "avatar" => $summary->avatarmedium,
            "rank" => $info->rank
        ));
    }

    echo json_encode($allMembers);
?>