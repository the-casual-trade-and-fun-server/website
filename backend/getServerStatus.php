<?php
    require __DIR__ . "/vars.php";
    header('Content-Type: application/json');

    if (!isset($DEMO_SERVER)) {
        $status = json_decode(file_get_contents("$SERVER_MANAGER_URL/serverStatus"));
    }
    else if ($DEMO_SERVER == -1) {
        $status = array(
            "online" => false,
            "reason" => "Timeout"
        );
    }
    else if ($DEMO_SERVER >= 0) {
        $names = array(
            "Dead4Ever",
            "xYMCx the god of destroy",
            "Fabi the angry blob",
            "Timo",
            "[FiverGang] Brickmaster 4000",
            "AngelDust",
            "Peter Pan",
            "OMG you should literaly run for your life HAHAHA, whatsup?",
            "I'm getting out of names what do I do?"
        );
        srand(time());
        $status = (object) array(
            "online" => true,
            "address" => "127.0.0.1:27015",
            "map" => "ctaf_land_v5",
            "maxPlayers" => 32,
            "players" => array_map(
                function () use($names) { return array("name" => $names[rand(0, 8)], "score" => rand(0, 200), "duration" => rand(0, 10000)); },
                array_fill(0, $DEMO_SERVER, null)
            )
        );
    }

    $map = "/assets/maps/$status->map/thumbnail.jpg";
    $status->image =
        file_exists(__DIR__ . "/..$map")
            ? $map
            : "/assets/maps/nomap.jpg";

    echo
        $status != null
            ? json_encode($status)
            : "offline";
?>