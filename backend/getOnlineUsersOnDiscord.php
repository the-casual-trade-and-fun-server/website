<?php
    require __DIR__ . "/vars.php";
    header('Content-Type: application/json');

    if (!isset($DISCORD_SERVER_ID)) {
        echo json_encode(array(
            "online" => random_int(0, 500),
            "invite" => "https://discordapp.com"
        ));
    }
    else {
        $data = file_get_contents("https://discordapp.com/api/guilds/$DISCORD_SERVER_ID/widget.json");
        if ($data == null) {
            echo "";
        }
        else {
            $discord = json_decode($data);
            echo json_encode(array(
                "online" => $discord->presence_count,
                "invite" => $discord->instant_invite
            ));
        }
    }
?>