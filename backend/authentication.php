<?php
    session_start(array("cookie_lifetime" => 432000));
    header('Content-Type: application/json');
    echo json_encode(
        isset($_SESSION["auth"])
            ? $_SESSION["auth"]
            : array("type" => "not-logged-in")
    );
?>