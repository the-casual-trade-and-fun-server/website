<?php
    include_once __DIR__ . "/vars.php";
    include_once __DIR__ . "/roles.php";
    include_once __DIR__ . "/postRequest.php";
    include_once __DIR__ . "/steamWebApi.php";

    class CommandHandler {
        public $neededRole;
        public $endpoint;
        private $_args;

        public function __construct(string $neededRole, string $endpoint, callable $args) {
            $this->neededRole = $neededRole;
            $this->endpoint = $endpoint;
            $this->_args = $args;
        }

        public function args($body) {
            return call_user_func($this->_args, $body);
        }
    }

    // COMMANDS
    $commandHandlers =
        array(
            "declareUserAsMember" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "addMember",
                function($body) { return array("user" => $body->user, "rank" => $body->rank); }
            ),
            "setRankOfMember" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "setMemberRank",
                function($body) { return array("user" => $body->member, "newRank" => $body->rank); }
            ),
            "removeMember" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "removeMember",
                function($body) { return array("user" => $body->member); }
            ),

            "giveUserVip" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "giveUserVIP",
                function($body) { return array("user" => $body->user, "date" => time(), "duration" => ($body->duration * 1440)); }
            ),
            "extendTime" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "extendVIPDuration",
                function($body) { return array("user" => $body->user, "dateOfExtension" => time(), "additionalDuration" => ($body->duration * 1440)); }
            ),
            "correctTime" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "correctVIPDuration",
                function($body) { return array("user" => $body->user, "newDuration" => ($body->duration * 1440)); }
            ),
            "transferTime" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "transferTime",
                function($body) { return array("user" => $body->user, "target" => $body->target, "date" => time(), "duration" => $body->duration); }
            ),
            "pauseVip" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "pauseVIP",
                function($body) { return array("user" => $body->user, "pauseDate" => time()); }
            ),
            "resumeVip" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "resumeVIP",
                function($body) { return array("user" => $body->user, "resumeDate" => time()); }
            ),
            "removeDueToRuleBreaking" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "removeVIPDueToRuleBreaking",
                function($body) { return array("user" => $body->vip); }
            ),
            "extendTimeForAllVips" => new CommandHandler(
                $ROLE_SERVER_OWNER,
                "extendTimeForAllVIPs",
                function($body) { return array("user" => $body->user, "date" => time(), "duration" => $body->duration); }
            ),

            "setJoinsound" => new CommandHandler(
                $ROLE_VIP,
                "setJoinsound",
                function($body) { return array("user" => Steam64AsSteamId($body->user), "sound" => $body->sound); }
            ),
            "useNoJoinsound" => new CommandHandler(
                $ROLE_VIP,
                "useNoJoinsound",
                function($body) { return array("user" => Steam64AsSteamId($body->user)); }
            ),
            "setCustomChatColors" => new CommandHandler(
                $ROLE_VIP,
                "setCustomChatColors",
                function($body) { return array(
                    "user" => Steam64AsSteamId($body->user),
                    "tag" => $body->colors->tag,
                    "name" => $body->colors->name,
                    "chat" => $body->colors->chat);
                }
            )
        );
    // ^^^^^^^^^^

    $body = GetBodyData();

    $commandHandler = $commandHandlers[$body->command];
    if ($commandHandler == null) {
        header("STATUS: 404");
        echo "Unknown command handler $body->command";
        exit;
    }

    EnsureRightsTo($commandHandler->neededRole, $body->token);

    $res = GetPost(
        "$SERVER_MANAGER_URL/$commandHandler->endpoint",
        array_merge(
            $commandHandler->args($body->args),
            array("accessKey" => $SERVER_MANAGER_ACCESS_KEY)
        )
    );
    echo ResolveError($res)
?>