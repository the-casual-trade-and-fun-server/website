<?php
    function GetBodyData() {
        return json_decode(file_get_contents("php://input"));
    }

    function GetPost($url, $data) {
        $curl = curl_init($url);
        // ##EMPTY## - Hack to transmit `Maybe.Nothing` (null) to server
        $fields = http_build_query(array_map(function($a) { return $a ?? "##EMPTY##"; }, $data), '', '&');
        $fields = str_replace("=%23%23EMPTY%23%23", "", $fields);
        curl_setopt_array($curl, array(
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_RETURNTRANSFER => true
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    function ResolveError($result) {
        $res = json_decode($result);
        return $res == null
            ? "offline"
            : ($res->success ? "" : $res->error);
    }
?>