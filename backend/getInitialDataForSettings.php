<?php
    require __DIR__ . "/vars.php";
    require __DIR__ . "/helpers.php";
    require __DIR__ . "/steamWebApi.php";
    header('Content-Type: application/json');

    function JoinsoundOf($serverManagerUrl, $user)
    {
        $apiCall = "$serverManagerUrl/joinsoundOf?user=$user";
        return optionToNullable(json_decode(file_get_contents($apiCall)));
    }

    function CustomChatColorsOf($serverManagerUrl, $user)
    {
        $colors = json_decode(file_get_contents("$serverManagerUrl/customChatColorsOf?user=$user"));
        if ($colors == null)
        {
            echo "offline";
            exit;
        }
        return array(
            "tag" => optionToNullable($colors->Tag),
            "name" => optionToNullable($colors->Name),
            "chat" => optionToNullable($colors->Chat)
        );
    }

    $user = Steam64AsSteamId($_GET["user"]);
    $activeJoinsound = JoinsoundOf($SERVER_MANAGER_URL, $user);
    $definedJoinsounds = json_decode(file_get_contents("$SERVER_MANAGER_URL/definedJoinsounds"));
    $customChatColors = CustomChatColorsOf($SERVER_MANAGER_URL, $user);

    if ($definedJoinsounds == null || $customChatColors == null) {
        echo "offline";
        exit;
    }

    echo json_encode(array(
        "joinsound" => $activeJoinsound,
        "definedJoinsounds" => $definedJoinsounds,
        "customChatColors" => $customChatColors
    ));
?>