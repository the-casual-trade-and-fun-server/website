<?php
    require __DIR__ . "/vars.php";
    require __DIR__ . "/helpers.php";
    require __DIR__ . "/steamWebApi.php";
    header('Content-Type: application/json');

    $loadedVips = file_get_contents("$SERVER_MANAGER_URL/getVIPs");
    if ($loadedVips == null) {
        echo "offline";
        exit;
    }

    $vips =
        asHashSet(
            function($vip) { return SteamIdAsSteam64($vip->id); },
            json_decode($loadedVips)
        );

    $vipsToFetch = array_keys($vips);
    $summaries =
        asHashSet(
            function($summary) { return $summary->steamid; },
            isset($DEMO_USER) ? GetDemoSummaries($vipsToFetch) : GetPlayerSummaries($STEAM_API_KEY, $vipsToFetch)
        );

    $allVips = array();
    foreach ($vips as $key => $info)
    {
        $summary = $summaries[$key];
        array_push($allVips, array(
            "id" => $info->id,
            "name" => $summary->personaname,
            "avatar" => $summary->avatarmedium,
            "date" => $info->date,
            "duration" => $info->duration,
            "durationFromPauses" => $info->durationFromPauses,
            "countOfPauses" => $info->countOfPauses,
            "pausedSince" => optionToNullable($info->pausedSince)
        ));
    }

    echo json_encode($allVips);
?>