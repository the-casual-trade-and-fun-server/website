module.exports = {
  theme: {
    extend: {
      colors: {
        primary: {
          lightest: "var(--color-primary-lightest)",
          lighter: "var(--color-primary-lighter)",
          light: "var(--color-primary-light)",
          default: "var(--color-primary)",
          dark: "var(--color-primary-dark)",
          darker: "var(--color-primary-darker)",
          darkest: "var(--color-primary-darkest)"
        },
        secondary: {
          lightest: "var(--color-secondary-lightest)",
          lighter: "var(--color-secondary-lighter)",
          light: "var(--color-secondary-light)",
          default: "var(--color-secondary)",
          dark: "var(--color-secondary-dark)",
          darker: "var(--color-secondary-darker)",
          darkest: "var(--color-secondary-darkest)"
        },
        neutral: {
          lightest: "var(--color-neutral-lightest)",
          lighter: "var(--color-neutral-lighter)",
          light: "var(--color-neutral-light)",
          default: "var(--color-neutral)",
          dark: "var(--color-neutral-dark)",
          darker: "var(--color-neutral-darker)",
          darkest: "var(--color-neutral-darkest)"
        },
        dangerous: {
          lightest: "var(--color-dangerous-lightest)",
          lighter: "var(--color-dangerous-lighter)",
          light: "var(--color-dangerous-light)",
          default: "var(--color-dangerous)",
          dark: "var(--color-dangerous-dark)",
          darker: "var(--color-dangerous-darker)",
          darkest: "var(--color-dangerous-darkest)"
        },
        success: {
          lightest: "var(--color-success-lightest)",
          lighter: "var(--color-success-lighter)",
          light: "var(--color-success-light)",
          default: "var(--color-success)",
          dark: "var(--color-success-dark)",
          darker: "var(--color-success-darker)",
          darkest: "var(--color-success-darkest)"
        }
      },
      textColor: {
        lightest: "var(--color-text-lightest)",
        lighter: "var(--color-text-lighter)",
        light: "var(--color-text-light)",
        default: "var(--color-text)",
        discord: "var(--color-discord)",
        steam: "var(--color-steam)"
      },
      backgroundColor: {
        default: "var(--color-background)",
        light: "var(--color-background-light)",
        lighter: "var(--color-background-lighter)",
        frame: "var(--color-frame)"
      }
    }
  },
  variants: {
    backgroundColor: ["responsive", "hover", "focus", "odd" ]
  }
};
